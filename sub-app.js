import express from "express";
import { subApp } from "util-api/app.js"; // Add `./` if develop & test under same source
import routerBuilder from "util-api/utils/routerBuilder.js"; // Add `./` if develop & test under same source

/*
 *  Sub-module Sample
 *  See TODO for procedures.
 *
 *  TODO Minimal packages installed
 *    Express
 */

// TODO router from express
const router = express.Router();
const builder = routerBuilder(router);

const app = subApp({
  route: {
    beforeRoute: (app) => {
      console.log("Customs routes override default routes");

      // TODO Directories structure
      /*
       *   /middlewares
       *   /modules
       *     main logics, collection of module classes
       *   /routes
       *   /utils
       *     utilities
       */

      // TODO Build collections of routes into router
      // See ./util-api/routes/*.routes.js
      // middlewares, module classes should be applied here
      router.get("/", (req, res) => {
        console.log(`Sample api endpoint for sub-module`);
        res.send("sample api");
      });

      // TODO use builder for modules
      // someModule.init(builder);

      // TODO Assign routers with path into Express app instance
      // See ./util-api/routes/routes.js
      app.use("/pre-route", router);
    },
    afterRoute: (app) => {
      console.log("Customs routes overrided by default routes");

      // same as beforeRoute()
    },
  },
});

export { app };
