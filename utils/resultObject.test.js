import resultObject from "./resultObject.js";

describe("resultObject, result", () => {
  // Success
  it("Should return object, 'success'", () => {
    const data = resultObject({ ok: true });
    expect(data).toStrictEqual({
      data: {
        ok: true,
      },
      message: null,
      status: "success",
    });
  });
  it("Should return object, 'success' with message", () => {
    const data = resultObject({ ok: true }, false, "message");
    expect(data).toStrictEqual({
      data: {
        ok: true,
      },
      message: "message",
      status: "success",
    });
  });

  // Error
  it("Should return object, 'error'", () => {
    const data = resultObject(false, { ok: true });
    expect(data).toStrictEqual({
      data: false,
      "error": {
        ok: true,
      },
      message: null,
      status: "error",
    });
  });
  it("Should return object, 'error' with no message on data", () => {
    const data = resultObject(false, { ok: true }, "message");
    expect(data).toStrictEqual({
      data: false,
      error: {
        ok: true,
      },
      message: "message",
      status: "error",
    });
  });
  it("Should return object, 'error' with message on data", () => {
    const data = resultObject(false, { ok: true }, "message", true);
    expect(data).toStrictEqual({
      data: {
        ok: true,
      },
      error: null,
      message: "message",
      status: "error",
    });
  });
});
