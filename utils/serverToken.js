import jwt from "jsonwebtoken";
import jwtDecode from "jwt-decode";
import fetch from "node-fetch";
import authService, { callbacks } from "../modules/auth/services/auth.service.js";

const refreshOnExpired = false;

const refreshObject = auth => ({ auth, expired: true });

/* istanbul ignore next */
const SERVER_SECRET_TOKEN = process.env.NODE_ENV === "test" ? "test_secret" : process.env.SERVER_SECRET_TOKEN;
/* istanbul ignore next */
if (!SERVER_SECRET_TOKEN && process.env.JEST_WORKER_ID === undefined) {
  console.error("\x1b[31m[Error] Invalid server secret\x1b[0m");
  console.error("App exit");
  process.exit(1);
}

// Control of providers, `authService` only here
const patchAndVerifyAccessTokenRequest = async req => {
  const { body } = req;
  // Mandatory fields
  let params = {
    platform: body?.platform ? body.platform : "web",
    environment: body?.environment ? body.environment : "production",
    provider: body.provider,
  };
  if (callbacks?.beforeFilter && typeof callbacks.beforeFilter === "function") params = callbacks.beforeFilter("patchAndVerifyAccessTokenRequest", params);

  if (params.provider === "firebase") {
    // Check token with firebase admin
    return await authService.firebaseAdminAuth(req, { ...params, token: body.token, package: body.package, device: body.device });
  }
  if (params.provider === "firebase-remote") {
    const { appId, token: customToken } = { ...params, appId: body.appId };
    // TODO `revamp_remoteAuthApp` move checking into authService.remoteAuthApp()
    // Check app registered and its source
    const exchangeCustomTokenEndpoint = authService.remoteAuthApp(appId);

    const { idToken } = await fetch(exchangeCustomTokenEndpoint, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
        token: customToken,
        returnSecureToken: true,
      }),
    })
    // .then((res) => { console.log(res); return res.json(); });
    .then((res) => res.json());

    if (
      !idToken
      && process.env.NODE_ENV !== "localhost" // by-pass localhost
    ) return false;
    body.token = idToken;
    return { ...params, appId: body.appId, token: body.token };
  }

  // else Auth field, self authService
  const user = await authService.authUserParams(req, body);
  if (user && Object.keys(user).length > 0) params.user = user;
  else return false;

  if (Object.values(params).some(v => !v)) return false; // Check if empty key-values

  // Verify params

  return params;
};

const generateAccessToken = async req => {
  let params = await patchAndVerifyAccessTokenRequest(req); // parse related providers
  if (Object.keys(params).length < 1) return "";
  params.user.internalUser = false;
  if (callbacks?.afterFilter && typeof callbacks.afterFilter === "function") params = callbacks.afterFilter("generateAccessToken", params);
  if (params.provider === "firebase" && params.user.internalUser === true) { // token from firebase verify
    // Map internal user
    if (params.token.user_id) {
      authService.init(req);
      params.user = await authService.getAuthUserInAuthUid(params.token.user_id, params.token);
    }
    if (params.user === false) return ""; // Handle specify `false` exception
    if (!params.user) params.user = {};
  }
  return jwt.sign(params, SERVER_SECRET_TOKEN, { expiresIn: "1d" });
};

const verifyAccessToken = token => {
  try {
    return { auth: jwt.verify(token, SERVER_SECRET_TOKEN) };
  } catch (exception) {
    // Handle expired
    if (exception?.expiredAt) {
      const auth = jwtDecode(token);
      return refreshObject(auth);
    }
    return false;
  }
};

const authenticateToken = async req => {
  const token = getAccessTokenFromRequest(req);

  // Verify jwt
  const auth = verifyAccessToken(token);
  if (!auth) return "";
  if (auth.refresh) {
    const refreshToken = {
      auth: auth.auth, // Complete auth object
      refresh: true,
    };
    if (refreshOnExpired) refreshToken.refreshToken = await generateAccessToken({ app: req.app, body: auth.auth });
    return refreshToken;
  }

  // By-pass on developement
  if (process.env.NODE_ENV === "localhost") return auth;

  return auth;
};

const getAccessTokenFromRequest = req => {
  const token = req.headers["x-access-token"] || req.headers.authorization;
  const regex = /^Bearer\s+/;
  return token && token.match(regex) ? token.replace(regex, "") : "";
};

export const authProviders = [
  "auth", // auth user db
  "firebase", // firebase custom token
  "firebase-remote", // registered firebase backend
];

export const verifyAndAccessTokenGateway = async req => {
  let { body } = req;
  // Filter data
  if (callbacks?.beforeFilter && typeof callbacks.beforeFilter === "function") body = callbacks.beforeFilter("verifyAndAccessTokenGateway", body);

  const provider = authProviders.indexOf(body.provider) >= 0 ? body.provider : "auth"; // Check existing in `authProviders`

  // Check auth header
  const auth = await authenticateToken(req);
  if (auth) return auth;

  switch (provider) {
    case "firebase":
      return accessTokenFirebase(req);
    case "firebase-remote":
      //return accessTokenRemote(req); // TODO to be fixed
    case "auth":
    default:
      req.body.provider = "auth";
      return accessTokenAuth(req);
  }
};

const accessTokenAuth = async req => {
  return { auth: await generateAccessToken(req) };
};

const accessTokenFirebase = async req => {
  return { auth: await generateAccessToken(req) };
};

const accessTokenRemote = async req => {
  // Patch body
  req.body = {
    provider: "firebase-remote",
    platform: "server",
    environment: req.body.environment || "production",
    appId: req.body.appId, // server-side registered app id
    token: req.body.token || "", // custom token
  };

  return { auth: await generateAccessToken(req) };
};
