import { getAuth } from "firebase-admin/auth";
import database from "../config/database.js";
import { authProviders } from "../modules/auth/models/auth.model.js";

const firebaseAuth = {
  getAdmin: (app, data) => {
    // Get admin by package
    const { platform = "web", environment = "production", package: packageId } = data;

    // Get externals
    let authAdmin = database.firebaseAdminExternal.get(app)[`${platform}-${environment}-${packageId}`];
    // Get primary
    if (!authAdmin) authAdmin = database.firebaseAdmin.get(app);

    return authAdmin;
  },
  adminVerifying: async (app, data) => {
    if (process.env.NODE_ENV === "localhost") { // By-pass user auth when localhost
      // Essential dummy user auth data
      const unixtimestamp = Math.floor(new Date() / 1000);
      data.token = {
        userId: "user id",
        provider_id: "provider",
        iss: "https://securetoken.google.com/<package>",
        aud: "<package>",
        auth_time: unixtimestamp,
        user_id: "auth uid",
        sub: "auth uid",
        iat: unixtimestamp,
        exp: unixtimestamp + 86400, // Map dummy always valid, 1 day
        firebase: {
          identities: {},
          sign_in_provider: "provider",
        }
      };
      data.user = { // Base user data
        id: data.token.userId,
      };
      return data;
    }

    const authAdmin = firebaseAuth.getAdmin(app, data);
    const { token: idToken = "" } = data;

    let authToken = false;
    try {
      authToken = await getAuth(authAdmin).verifyIdToken(idToken);
      data.token = authToken; // Transform token into user auth data
      data.user = { // Base user data
        id: authToken.userId,
      };
      return data;
    } catch (ex) {
      console.log(ex);
      return false;
    }

    return false;
  },
  buildAuthData: (uid, provider) => {
    const providerStr = Object.keys(authProviders).filter(v => authProviders[v].provider === provider).shift();
    return {
      provider: providerStr,
      authUid: uid,
    };
  },
};

export default firebaseAuth;
