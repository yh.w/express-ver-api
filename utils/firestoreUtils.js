export const getDocuments = async (db, queries) => {
  const {
    collectionName,
    where,
    orderBy,
  } = queries;

  if (!collectionName) return null;
  let doc = await db.collection(collectionName);

  if (where) {
    if (!where.field || !where.condition || !where.value) return null;
    if (where.field === "id" || where.field === "_id") {
      where.field = "__name__";
      if (orderBy && orderBy.field) orderBy.field = "__name__";
    }
    doc = await doc.where(where.field, where.condition, where.value);
  }

  if (orderBy && orderBy.field && orderBy.sort) {
    doc = await doc.orderBy(orderBy.field, orderBy.sort);
  }
  doc = await doc.get();

  const data = await doc._docs();
  const newData = data.map(v => {
    const data = v.data();
    data.id = v.id;
    if (data.createDatetime) data.createDatetime = data.createDatetime.toDate();
    if (data.updateDatetime) data.updateDatetime = data.updateDatetime.toDate();
    return data;
  }); // Parse to json
  return newData;
};

/* istanbul ignore next */
export const upsertDocument = async (db, collectionName, data, update = false) => {
  if (!collectionName || !data) return null; // Should not process when empty collection or data

  // Data field operations
  const { id } = data;
  delete data.id;
  delete data._id;

  // Check is update valid
  const where = id ? { field: "id", condition: "==", value: id } : null;
  const docs = await getDocuments(db, { collectionName, where })
  if (docs.length < 1) update = false;

  // Do not change some fields
  if (update) {
    if (data.createdBy) delete data.createdBy;
    if (data.createDatetime) delete data.createDatetime;
  }

  let result = false;
  try {
    result = await setDocument(db, collectionName, id, data, update);
  } catch (ex) {
    // Ignore on jest coverage
    /* istanbul ignore next */
    console.log("`upsertDocument()` exception:");
    /* istanbul ignore next */
    console.log(ex);
  }

  if (result && result._path?.segments[1]) data.id = result._path.segments[1]; // eslint-disable-line prefer-destructuring
  else if (update && result && result._writeTime) { // Handle update
    data = { ...docs[0], ...data } ;
    data.id = id;
  }
  else data = null;

  return data;
};

export const setDocument = async (db, collectionName, docId, data, update = false) => {
  if (!collectionName || !data) return null;

  // Handle events
  if (
    !docId
    || (docId && !update)
  ) {
    if (typeof firestoreUtils.events.onCreate === "function") data = firestoreUtils.events.onCreate(data);
  } else {
    if (typeof firestoreUtils.events.onUpdate === "function") data = firestoreUtils.events.onUpdate(data);
  }

  if (!docId) return await addDocument(db, collectionName, data);
  return update ? await getDocumentRef(db, collectionName).doc(docId).update(data) : await getDocumentRef(db, collectionName).doc(docId).set(data);
};

export const addDocument = async (db, collectionName, data) => {
  if (!collectionName || !data) return null;
  return await getDocumentRef(db, collectionName).add(data);
};

export const getDocumentRef = (db, collectionName) => db.collection(collectionName);

const firestoreUtils = {
  getDocuments,
  upsertDocument,
};
export default firestoreUtils;
