import "regenerator-runtime/runtime";
import asyncMiddleware from "./asyncMiddleware.js";

describe("asyncMiddleware, return resolved Promise", () => {
  it("Should return resolved Promise", async () => {
    const mockFunction = jest.fn();
    const req = jest.fn();
    const res = jest.fn();
    const next = jest.fn();
    const data = asyncMiddleware(mockFunction)(req, res, next);

    expect(mockFunction).toBeCalled();
  });
});
