const logger = {
  debug: (msg) => {
    // log only when develop
    if (process.env.NODE_ENV === "localhost") {
      console.log(msg);
    }
  }
};
export default logger;
