import mongoose from "mongoose";
import resultObject, { stateConstant } from "../utils/resultObject.js";
import firestoreOrm from "../utils/orm/firestoreOrm.js";
import mongooseOrm from "../utils/orm/mongooseOrm.js";
import database from "../config/database.js";

const { Schema } = mongoose;

export const generalFields = {
  id: {
    type: String,
  },
  status: {
    type: String,
    enum: {
      values: ["active", "inactive", "deleted"],
      message: "{VALUE} is not supported",
    },
    default: "active",
    required: true,
  },
  createdBy: {
    type: String,
    default: "SYSTEM",
    required: true,
  },
  createDatetime: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedBy: {
    type: String,
    default: "SYSTEM",
    required: true,
  },
  updateDatetime: {
    type: Date,
    default: Date.now,
    required: true,
  },
};

export const validator = {
  emailRegex: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
};

class ModelClass {
  constructor () {
    // For connection
    this.dataSource = "";
    this.schema = null;
    this.db = null;
    this.init = req => {
      if (!this.isInit()) return false;
      this.db = database[this.dataSource].get(req.app);
      if (!this.schema) this.schema = new Schema(generalFields);
    };
    this.isInit = () => this.dataSource && this.collection && this.schema; // To block if not significant configs
    this.conn = () => {
      if (this.dataSource === "firestore") {
        firestoreOrm.events = this.events;
        return firestoreOrm;
      }
      if (this.dataSource === "mongoose") {
        mongooseOrm.events = this.events;
        return mongooseOrm(this.schema);
      }
    };
    this.collection = "";
    // For schema
    this.generalFields = generalFields;
    // General
    this.get = async (queries, options = {}) => {
      if (!this.isInit()) return resultObject(false, stateConstant.ERROR_FORBIDDEN);

      if (!options.raw && this.viewSuffix) {
        const collectionView = this.collectionView();
        const data = await this.conn().getDocuments(this.db, { collectionName: collectionView, ...queries }, options);
        if (data && data.length > 0) {
          return resultObject(
            data.map(d => {
              this.hasAssociations(false).forEach(v => d[v] = this.conn().packDocument(d[v])); // pack associations
              return !options.raw && typeof this.events.afterRetrive === "function"
                ? this.events.afterRetrive(d) // Apply afterRetrive
                : d;
            })
          ); // In case no view exist
        }
      }

      let data = {};
      data = await this.conn().getDocuments(this.db, { collectionName: this.collection, ...queries }, options);

      if (data && data.length > 0 && !options.raw && typeof this.events.afterRetrive === "function") data = data.map(v => this.events.afterRetrive(v));

      return resultObject(data);
    };
    this.upsert = async (data, options = {}) => {
      if (!data) return resultObject(false, stateConstant.ERROR_DATA_MISSED);
      if (!this.isInit()) return resultObject(false, stateConstant.ERROR_FORBIDDEN);
      data = this.conn().packDocument(data);

      // Accept id only
      if (data._id) {
        data.id = data._id;
        delete data._id;
      }

      // Modify schema
      const schema = this.schema;
      if (data.id) options.update = true;
      if (!options?.update) {
        schema.remove("_id");
      }

      if (typeof this.events.onPackData === "function") data = this.events.onPackData(data);

      const Model = mongoose.model(`${this.collection}Model`, schema);
      const object = new Model(data);

      const validation = object.validateSync();
      if (validation) {
        console.log("Model validateSync()", validation);
        return resultObject(false, stateConstant.ERROR_DATA_INVALID, validation._message);
      };
      const upsertData = object.toObject();
      // Set back data id for update action
      delete upsertData._id;
      if (options?.update && data.id) upsertData.id = data.id;

      // check unique
      if (options?.unique) {
        let field = "";
        let value = null;
        if (options?.unique?.field) {
          field = typeof options.unique.field === "string" ? options.unique.field : "id"; // Accept only string with separator of `.`
          value = data;
          options.unique.field.split(".").map(v => { value = value[v]; }); // Get nested value
        } else {
          // Set default unique field
          field = "id";
          value = upsertData.id;
        }
        // Only on update
        const exist = await this.get({
          where: {
            field,
            condition: "==",
            value,
          }
        }, { raw: true });

        if (
          exist?.status !== "success"
          || (!options?.update && exist.data?.length > 0) // On insert should not exist
          || (options?.update && ( // On update
            (field === "id" && exist.data?.length !== 1) // unique id, should have one, self-row, only
            || (field !== "id" && exist.data?.length > 0) // else unique, should not exist
          ))
        ) {
          return resultObject(false, stateConstant.ERROR_DATA_INVALID);
        }

        // Remove here-only flag `unique`
        delete options.unique;
      }

      // Handle parent
      let result = {};
      result = await this.conn().upsertDocument(this.db, this.collection, upsertData, options.update, options);

      // Handle associations
      if (this.hasAssociations()) {
        if (!options?.update) {
          // On create
          // Prepare assoication on new documents
          this.hasAssociations(false).forEach(v => {
            const { foreignKey } = this.associations[v];
            if (data[v]) { // In case data received contain associated key
              if (Object.keys(data[v]).length < 1) data[v] = {};
              data[v][foreignKey] = result.id;
            }
          });
        }

        // upsert children in parallel
        await Promise.all(this.hasAssociations(false).map(async v => {
          const { foreignKey } = this.associations[v];
          const childData = data[v];
          if (!childData) return;

          // Need set back model db
          this.associations[v].model.db = this.db;

          // Build data
          const childDoc = await this.associations[v].model.get({
            where: { field: this.associations[v].foreignKey, condition: "==", value: data.id },
          });

          let childResult = {};
          if (options?.update) {
            childData[foreignKey] = (childDoc.data[0] || data || {}).id;
            childResult = await this.associations[v].model.upsert(childData, { update: true });
          } else {
            childResult = await this.associations[v].model.upsert(childData, { update: false });
          }

          result[v] = childResult.data;
        }));

        // Upsert view, uid should same with parent uid
        const collectionView = this.collectionView();
        const viewResult = await this.conn().upsertDocument(this.db, collectionView, result, true);
        // If view save failed
        if (!viewResult) {
          return resultObject(false, result, "Invalid view", true);
        }
        result = viewResult;
      }

      if (typeof this.events.afterRetrive === "function") result = this.events.afterRetrive(result);

      return result ? resultObject(result) : resultObject(false, stateConstant.ERROR_DATA_NOT_FOUND);
    };
    /*
     *  Model features
     */
    // Events
    this.events = {
      onPackData: data => data, // before upsert
      onCreate: data => data, // TODO not implemented
      onUpdate: data => data, // TODO not implemented
      onDelete: data => data, // TODO not implemented
      afterRetrive: data => data,
    };
    // Association models, will be created and stored new collection with prefix `views_` for display and overrided when parent models updated
    this.associations = {};
    this.hasAssociations = (bool = true) => bool ? Object.keys(this.associations).length > 0 : Object.keys(this.associations);
    this.collectionView = () => `views_${this.viewSuffix || this.collection}`; // Views collection
  };
};

const buildModel = ({
  events,
  associations,
  dataSource,
  collection,
  viewSuffix,
  schema,
}) => {
  const newModel = new ModelClass();
  newModel.events = { ...newModel.events, ...events };
  newModel.associations = associations || {};
  newModel.dataSource = dataSource;
  newModel.collection = collection;
  newModel.viewSuffix = viewSuffix;
  newModel.schema = !schema || !schema.instanceOfSchema
    ? new Schema({
      ...generalFields,
      ...schema
    }, {
      _id: false,
      minimize: false,
    })
    : schema;
  return newModel;
};

export default buildModel;
