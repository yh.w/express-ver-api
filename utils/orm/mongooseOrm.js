import mongoose from "mongoose";

export const getDocuments = async (db, queries) => {
};
export const upsertDocument = async (db, collectionName, data, update = false) => {
};

const mongooseOrm = {
  model: null,

// TODO
// 1. create from mongoose package Model from Schema
// 2. build getDocuments()
  getDocuments: () => {},
// 3. build upsertDocument()
  upsertDocument: () => {},

};

const mongooseOrmInit = schema => {
  // Create Model from Schema for saving
  const Model = mongoose.model("Model", schema);
  mongooseOrm.model = Model;

  return mongooseOrm;
};

export default mongooseOrmInit;
