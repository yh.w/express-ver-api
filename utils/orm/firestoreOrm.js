export const getDocuments = async (db, queries, options = {}) => {
  const {
    collectionName,
    where,
    orderBy,
  } = queries;

  if (!collectionName) return null;
  let doc = await db.collection(collectionName);

  if (where) {
    if (!where.field || !where.condition || !where.value) return null;
    if (where.field === "id" || where.field === "_id") {
      where.field = "__name__";
      if (orderBy && orderBy.field) orderBy.field = "__name__";
    }
    doc = await doc.where(where.field, where.condition, where.value);
  }

  if (
    orderBy && orderBy.field && orderBy.sort
    && (!where || orderBy.field === where.field) // firestore default(not indexed) not support different fields on where & orderBy
  ) {
    doc = await doc.orderBy(orderBy.field, orderBy.sort);
  }
  if (options?.activeOnly) doc = await doc.where("status", "==", "active");
  doc = await doc.get();

  const data = await doc._docs();
  const newData = data.map(v => {
    const data = v.data();
    data.id = v.id;
    return packDocument(data);
  }); // Parse to json
  return newData;
};

/* istanbul ignore next */
export const upsertDocument = async (db, collectionName, data, update = false, options = {}) => {
  if (!collectionName || !data) return null; // Should not process when empty collection or data
  data = packDocument(data);

  // Data field operations
  const { id } = data;
  delete data.id;
  delete data._id;

  // Check is update valid
  const where = id ? { field: "id", condition: "==", value: id } : null;
  const docs = await getDocuments(db, { collectionName, where });
  if (docs.length < 1) {
    if (options?.updateOnly) {
      // abort update since no matching id found
      return null;
    }
    update = false;
  }

  // Do not change some fields
  if (update) {
    if (data.createdBy) delete data.createdBy;
    if (data.createDatetime) delete data.createDatetime;
  }

  let result = false;
  try {
    result = await setDocument(db, collectionName, id, data, update);
  } catch (ex) {
    // Ignore on jest coverage
    /* istanbul ignore next */
    console.log("`upsertDocument()` exception:");
    /* istanbul ignore next */
    console.log(ex);
  }

  if (result && result._path?.segments[1]) data.id = result._path.segments[1]; // eslint-disable-line prefer-destructuring
  else if (result && result._writeTime) { // Handle update
    data = { ...docs[0], ...data };
    data.id = id;
  } else data = null;

  return data;
};

export const setDocument = async (db, collectionName, docId, data, update = false) => {
  if (!collectionName || !data) return null;

  // Handle events
  if (
    !docId
    || (docId && !update)
  ) {
    if (firestoreOrm.events?.onCreate && typeof firestoreOrm.events.onCreate === "function") data = firestoreOrm.events.onCreate(data);
  } else {
    if (firestoreOrm.events?.onUpdate && typeof firestoreOrm.events.onUpdate === "function") data = firestoreOrm.events.onUpdate(data);
  }

  if (!docId) return await addDocument(db, collectionName, data);
  return update ? await getDocumentRef(db, collectionName).doc(docId).update(data) : await getDocumentRef(db, collectionName).doc(docId).set(data);
};

export const addDocument = async (db, collectionName, data) => {
  if (!collectionName || !data) return null;
  return await getDocumentRef(db, collectionName).add(data);
};

export const getDocumentRef = (db, collectionName) => db.collection(collectionName);

const packDocument = (data) => {
  if (data?.createDatetime && data.createDatetime.toDate) data.createDatetime = data.createDatetime.toDate();
  if (data?.updateDatetime && data.updateDatetime.toDate) data.updateDatetime = data.updateDatetime.toDate();
  return data;
};

const firestoreOrm = {
  getDocuments,
  upsertDocument,
  packDocument,
};
export default firestoreOrm;
