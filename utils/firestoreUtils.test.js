import "regenerator-runtime/runtime";
import { getDocuments, upsertDocument, setDocument, addDocument, getDocumentRef } from "./firestoreUtils.js";

// Prepare mock functions
const docs = [
  { id: "dummyId", data: () => ({}) },
];
const _docs = jest.fn(() => (docs));
const docGet = jest.fn(() => ({
  _docs,
}));
const docSet = jest.fn();
const whereOrderBy = jest.fn((field, sort) => ({ get: docGet }));
const colWhere = jest.fn((field, condition, value) => ({
  orderBy: whereOrderBy,
  get: docGet,
  set: docSet,
}));
const colAdd = jest.fn((data) => {});
const colDoc = jest.fn(docId => ({ set: docSet }));
const collection = jest.fn(collectionName => ({
  where: colWhere,
  add: colAdd,
  doc: colDoc,
}));
const db = {
  collection: collection,
};

beforeEach(() => {
  collection.mockClear();
  colWhere.mockClear();
  colAdd.mockClear();
  colDoc.mockClear();
  whereOrderBy.mockClear();
  docGet.mockClear();
  docSet.mockClear();
  _docs.mockClear();
});

describe("firestoreUtils functions", () => {
  it("getDocuments(), should return array", async () => {
    const queries = {
      collectionName: "dummy",
      where: {
        field: "whereField",
        condition: "=",
        value: "test",
      },
      orderBy: {
        field: "sortField",
        sort: "asc",
      },
    };
    const data = await getDocuments(db, queries);
    expect(collection).toBeCalledWith(queries.collectionName);
    expect(colWhere).toBeCalled();
    expect(whereOrderBy).toBeCalled();
    expect(docGet).toBeCalled();
    expect(_docs).toBeCalled();
    expect(docs.length).toEqual(data.length);
  });
  it("getDocuments(), should return null, on empty collection", async () => {
    const queries = {
      collectionName: "",
      where: {
        field: "whereField",
        condition: "=",
        value: "test",
      },
      orderBy: {
        field: "sortField",
        sort: "asc",
      },
    };
    const data = await getDocuments(db, queries);
    expect(collection).not.toBeCalled();
    expect(colWhere).not.toBeCalled();
    expect(whereOrderBy).not.toBeCalled();
    expect(docGet).not.toBeCalled();
    expect(_docs).not.toBeCalled();
    expect(data).toEqual(null);
  });
  it("getDocuments(), should return null, on empty where", async () => {
    const queries = {
      collectionName: "dummy",
      where: {},
      orderBy: {
        field: "sortField",
        sort: "asc",
      },
    };
    const data = await getDocuments(db, queries);
    expect(collection).toBeCalled();
    expect(colWhere).not.toBeCalled();
    expect(whereOrderBy).not.toBeCalled();
    expect(docGet).not.toBeCalled();
    expect(_docs).not.toBeCalled();
    expect(data).toEqual(null);
  });
  it("getDocuments(), should return array, on empty orderBy", async () => {
    const queries = {
      collectionName: "dummy",
      where: {
        field: "whereField",
        condition: "=",
        value: "test",
      },
      orderBy: {},
    };
    const data = await getDocuments(db, queries);
    expect(collection).toBeCalled();
    expect(colWhere).toBeCalled();
    expect(whereOrderBy).not.toBeCalled();
    expect(docGet).toBeCalled();
    expect(_docs).toBeCalled();
    expect(data.length).toEqual(docs.length);
  });
  it("upsertDocument(), should return object, with data id", async () => {
    const collectionName = "dummy";
    const id = "dummyId";
    const input = { id, data: "ok" };
    await upsertDocument(db, collectionName, input);
    expect(collection).toBeCalled();
    expect(colDoc).toBeCalledWith(id);
    expect(docSet).toBeCalledWith(input);
  });
  it("upsertDocument(), should return object, with data id", async () => {
    const collectionName = "dummy";
    const input = { data: "ok" };
    const data = await upsertDocument(db, collectionName, input);
    expect(collection).toBeCalled();
    expect(colDoc).not.toBeCalled();
    expect(colAdd).toBeCalledWith(input);
  });
  it("upsertDocument(), should return null, on empty collectionName", async () => {
    const collectionName = "";
    const input = { data: "ok" };
    const data = await upsertDocument(db, collectionName, input);
    expect(data).toEqual(null);
  });
  it("upsertDocument(), should return null, on empty data", async () => {
    const collectionName = "dummy";
    const input = null;
    const data = await upsertDocument(db, collectionName, input);
    expect(data).toEqual(null);
  });
  it("setDocument(), should call getDocumentRef()", async () => {
    const collectionName = "dummy";
    const docId = "docId";
    const input = { data: "ok" };
    const data = await setDocument(db, collectionName, docId, input);
    expect(collection).toBeCalledWith(collectionName);
    expect(colDoc).toBeCalledWith(docId);
    expect(docSet).toBeCalledWith(input);
  });
  it("setDocument(), should return null, on empty collectionName", async () => {
    const collectionName = "";
    const docId = "docId";
    const input = { data: "ok" };
    const data = await setDocument(db, collectionName, docId, input);
    expect(collection).not.toBeCalled();
    expect(data).toEqual(null);
  });
  it("setDocument(), should return null, on empty data", async () => {
    const collectionName = "dummy";
    const docId = "docId";
    const input = null;
    const data = await setDocument(db, collectionName, docId, input);
    expect(collection).not.toBeCalled();
    expect(data).toEqual(null);
  });
  it("setDocument(), should call addDocument(), on empty docId", async () => {
    const collectionName = "dummy";
    const docId = "";
    const input = { data: "ok" };
    const data = await setDocument(db, collectionName, docId, input);
    expect(collection).toBeCalledWith(collectionName);
    expect(colAdd).toBeCalled();
  });
  it("addDocument(), should return collection.add", async () => {
    const collectionName = "dummy";
    const input = { data: "ok" };
    const data = await addDocument(db, collectionName, input);
    expect(collection).toBeCalledWith(collectionName);
    expect(colAdd).toBeCalledWith(input);
  });
  it("addDocument(), should return null, on empty collectionName", async () => {
    const collectionName = "dummy";
    const input = null;
    const data = await addDocument(db, collectionName, input);
    expect(collection).not.toBeCalled();
    expect(colAdd).not.toBeCalled();
    expect(data).toEqual(null);
  });
  it("addDocument(), should return null, on empty data", async () => {
    const collectionName = "dummy";
    const input = null;
    const data = await addDocument(db, collectionName, input);
    expect(collection).not.toBeCalled();
    expect(colAdd).not.toBeCalled();
    expect(data).toEqual(null);
  });
  it("getDocumentRef(), should return collection", async () => {
    const collectionName = "dummy";
    const data = await getDocumentRef(db, collectionName);
    expect(collection).toBeCalledWith(collectionName);
    expect(data).toEqual(collection());
  });
});
