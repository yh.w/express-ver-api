import reqUtil from "./reqUtil.js";
import { authFields } from "../middlewares/authorization.js";

const getAuthUserFromReq = () => {
  const { req } = reqUtil;
  let data = {};
  try {
    data = req[authFields.reqField].auth.auth;
  } catch (ex) {
  }
  return data;
};

export default getAuthUserFromReq;
