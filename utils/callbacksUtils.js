// Base callbacks
const baseCallbacks = {
  beforeFilter: (func = "", data) => data,
  afterFilter: (func = "", data) => data,
};

const utils = {
  getCallbacks: () => baseCallbacks,
  fetch: async (callbacks, callbackName = "", params = { state: "", data: null }) => {
    let callback;
    const result = params.data || null;
    try {
      callback = callbacks[callbackName];
    } catch (ex) {
      // In case not an valid object
      return result;
    }

    if (typeof callback !== "function") {
      return result;
    }

    if (callback[Symbol.toStringTag] === 'AsyncFunction') {
      // Run async pipeline
      return await callback(params.state, result);
    }

    return callback(params.state, result);
  },
};

export default utils;
