import "regenerator-runtime/runtime";
import jwt from "jsonwebtoken";
import { patchAccessTokenRequest, updateAndVerifyAccessToken } from "./serverToken.js";
// import { defaultPlatforms } from "../modules/platformModule.js";

describe("serverToken functions", () => {
  const dummyToken = "dummyToken";
  const throwToken = "throwToken";
  const invalidToken = "invalidToken";
  const invalidUidToken = "invalidUidToken";
  const invalidExpToken = "invalidExpToken";
  const resetReq = () => ({
    body: {
      token: "token",
      uid: "uid",
      platform: "ios",
      package: "package",
      device: "device",
      environment: "staging",
    },
  });
  const resetTokenReq = (token = "dummyToken") => ({
    headers: {
      "x-access-token": `Bearer ${token}`,
      authorization: `Bearer ${token}`,
    },
  });
  const jwtSignSpy = jest.spyOn(jwt, "sign").mockImplementation(() => dummyToken);
  const jwtVerifySpy = jest.spyOn(jwt, "verify").mockImplementation(token => {
    if (token === throwToken) throw new Error();
    if (token === invalidToken) return false;
    if (token) {
      const req = resetReq().body;
      req.token = {
        userId: token === invalidUidToken ? invalidUidToken : req.uid,
        exp: token === invalidExpToken ? 1000 : (Math.floor(new Date() / 1000) + 1000),
      };
      return req;
    }
    return false;
  });
  it("patchAccessTokenRequest(), should return object with limited fields", () => {
    // let req = {};

    // // Test general
    // req = resetReq();
    // expect(patchAccessTokenRequest(req)).toEqual(req.body);
    // // Test invalid platform
    // req = resetReq();
    // req.body.platform = "platform";
    // expect(patchAccessTokenRequest(req)).toEqual(false);
    // // Test empty fields
    // const fields = [
    //   "token",
    //   "uid",
    //   "platform",
    //   "package",
    //   "device",
    // ];
    // fields.forEach((v) => {
    //   req = resetReq();
    //   req.body[v] = "";
    //   expect(patchAccessTokenRequest(req)).toEqual(false);
    // });
  });
  it("updateAndVerifyAccessToken(), should return user auth object, on header x-access-token", () => {
    // const req = resetTokenReq();
    // req.headers = { "x-access-token": req.headers["x-access-token"] };
    // const data = updateAndVerifyAccessToken(req);
    // expect(data).toHaveProperty("token", "uid", "platform", "package", "device");
  });
  it("updateAndVerifyAccessToken(), should return user auth object, on header authorization", () => {
    // const req = resetTokenReq();
    // req.headers = { authorization: req.headers.authorization };
    // const data = updateAndVerifyAccessToken(req);
    // expect(data).toHaveProperty("token", "uid", "platform", "package", "device");
  });
  it("updateAndVerifyAccessToken(), should return token", () => {
    // const req = resetTokenReq();
    // req.headers = {};
    // req.body = resetReq().body;
    // const data = updateAndVerifyAccessToken(req);
    // expect(data).toEqual(dummyToken);
  });
  it("updateAndVerifyAccessToken(), should return empty token, on token for throw", () => {
    // const req = resetTokenReq();
    // req.headers = {
    //   authorization: `Bearer ${throwToken}`,
    // };
    // const data = updateAndVerifyAccessToken(req);
    // expect(data).toEqual("");
  });
  it("updateAndVerifyAccessToken(), should return empty token, on no req body", () => {
    // const req = resetTokenReq();
    // req.headers = {};
    // const data = updateAndVerifyAccessToken(req);
    // expect(data).toEqual("");
  });
  it("updateAndVerifyAccessToken(), should return empty token, on invalid req headers", () => {
    // const req = resetTokenReq();
    // req.headers = {
    //   authorization: `Bearer ${invalidToken}`,
    // };
    // const data = updateAndVerifyAccessToken(req);
    // expect(data).toEqual("");
  });
  it("updateAndVerifyAccessToken(), should return empty token, on invalid auth user object, uid", () => {
    // let data = "";
    // const req = resetTokenReq();
    // req.headers = {
    //   authorization: `Bearer ${invalidUidToken}`,
    // };
    // data = updateAndVerifyAccessToken(req);
    // expect(data).toEqual("");
  });
  it("updateAndVerifyAccessToken(), should return empty token, on invalid auth user object, exp (Disabled atm, return valid user object)", () => {
    // let data = "";
    // const req = resetTokenReq();
    // req.headers = {
    //   authorization: `Bearer ${invalidExpToken}`,
    // };
    // data = updateAndVerifyAccessToken(req);
    // // Disabled exp comparsion
    // // expect(data).toEqual("");
    // expect(data).toHaveProperty("token", "uid", "platform", "package", "device");
  });
});
