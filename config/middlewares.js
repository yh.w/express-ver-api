export default {
  helmet: {},
  cors: {
    origin: (origin, callback) => {
      if (process.env.NODE_ENV === "localhost") {
        callback(null, true);
        return;
      }

      const whitelist = [
        "http://localhost:3000",
        "http://localhost:3001",
        "http://localhost:8100",
        "chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop", // postman
        "chrome-extension://coohjcphdfgbiolnekdpbcijmhambjff", // tabbed postman
        "chrome-extension://coohjcphdfgbiolnekdpbcijmhambjff/index.html", // tabbed postman
        "decentr-extension://coohjcphdfgbiolnekdpbcijmhambjff", // tabbed postman
      ];

      if (
        !origin // Allow REST tools
        || whitelist.indexOf(origin) !== -1
      ) {
        callback(null, true);
      } else {
        callback(new Error(`Not allowed by CORS: ${origin}`));
      }
    },
  }
};
