import mongoose from "mongoose";
import admin from "firebase-admin";

// Get db from req shotcut
export const getDatabaseFromReq = req => req.app.get(req.app.get("databaseAlias"));

const configs = {
  mongoose: {
    host: process.env.MONGOOSE_HOSR || "",
    port: process.env.MONGOOSE_PORT || "",
    db: process.env.MONGOOSE_DB || "db",
  },
  firebase: {
    type: process.env.FIREBASE_TYPE || "",
    project_id: process.env.FIREBASE_PROJECT_ID || "",
    private_key_id: process.env.FIREBASE_PRIVATE_KEY_ID || "",
    private_key: JSON.parse(`"${process.env.FIREBASE_PRIVATE_KEY || ""}"`), // parse newline char
    client_email: process.env.FIREBASE_CLIENT_EMAIL || "",
    client_id: process.env.FIREBASE_CLIENT_ID || "",
    auth_uri: process.env.FIREBASE_AUTH_URI || "",
    token_uri: process.env.FIREBASE_TOKEN_URI || "",
    auth_provider_x509_cert_url: process.env.FIREBASE_AUTH_PROVIDER_X509_CERT_URL || "",
    client_x509_cert_url: process.env.FIREBASE_CLIENT_X509_CERT_URL || "",
  },
  bigquery: {
    type: process.env.BIGQUERY_TYPE || "",
    project_id: process.env.BIGQUERY_PROJECT_ID || "",
    private_key_id: process.env.BIGQUERY_PRIVATE_KEY_ID || "",
    private_key: process.env.NODE_ENV === "localhost" ? 
      (process.env.BIGQUERY_PRIVATE_KEY || "").replace(/\\n/g, '\n') // special handling in localhost
      : JSON.parse(`"${process.env.BIGQUERY_PRIVATE_KEY || ""}"`), // parse newline char
    client_email: process.env.BIGQUERY_CLIENT_EMAIL || "",
    client_id: process.env.BIGQUERY_CLIENT_ID || "",
    auth_uri: process.env.BIGQUERY_AUTH_URI || "",
    token_uri: process.env.BIGQUERY_TOKEN_URI || "",
    auth_provider_x509_cert_url: process.env.BIGQUERY_AUTH_PROVIDER_X509_CERT_URL || "",
    client_x509_cert_url: process.env.BIGQUERY_CLIENT_X509_CERT_URL || "",
  },
};

const database = {
  mongoose: {
    config: configs.mongoose,
    init: app => {
      const config = database.mongoose.config;
      if (config.host) { // Prevent empty host set
        mongoose.connect(`mongodb://${config.host}${config.port ? `:${config.port}` : ""}/${config.db}`);
        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "MongoDB connection error:"));
      }
    },
  },
  firestore: { // firestore connector only
    config: configs.firebase,
    init: app => {
      let db = {};
      let theAdmin = {};
      if (database.firestore.db) db = database.firestore.db; // Prevent multiple initialize admin
      else {
        try {
          theAdmin = admin.initializeApp({
            credential: admin.credential.cert(database.firestore.config),
          });
          db = admin.firestore();
          database.firestore.db = db;
        } catch (ex) {
          console.error("\x1b[31m[Error]\x1b[0m Firebase service may not work, inti error:", ex.message);
        }
      }
      app.set("firestore", db);
      app.set("databaseAlias", "firestore");

      // Set admin
      app.set("firebaseAdmin", theAdmin);
    },
    get: app => {
      let theFirestore = app.get("firestore"); // `app` from `req`, e.g. req.app
      if (!theFirestore) {
        database.firestore.init(app);
        theFirestore = app.get("firestore");
      }
      return theFirestore;
    },
    db: null, // Actually store an admin firestore object
  },
  bigquery: configs.bigquery,
  // Full admins
  firebaseAdmin: {
    get: app => {
      let theAdmin = app.get("firebaseAdmin"); // `app` from `req`, e.g. req.app
      if (!theAdmin) {
        database.firestore.init(app);
        theAdmin = app.get("firebaseAdmin");
      }
      return theAdmin;
    },
  },
  firebaseAdminExternal: {
    init: (app) => {
      const externalProjects = {};
      database.firebaseAdminExternal.projectPackages.forEach(v => {
        v.platforms.forEach(platform => {
          let adminNameSpace = `${platform}-${v.environment}-${v.id}`;

          try {
            // In case same package id is unique per each platforms
            const externalAdmin = admin.initializeApp({
              credential: admin.credential.cert(v.serviceAccountKey),
            }, adminNameSpace);
            externalProjects[adminNameSpace] = externalAdmin;
          } catch (ex) {
          }
        });
      });
      app.set("firebaseAdminExternal", externalProjects);
    },
    get: app => {
      let theAdmin = app.get("firebaseAdminExternal"); // `app` from `req`, e.g. req.app
      if (!theAdmin) {
        database.firebaseAdminExternal.init(app);
        theAdmin = app.get("firebaseAdminExternal");
      }
      return theAdmin;
    },
    projectPackages: [
      /* example
      {
        id: "dummy.app.id",
        platforms: ["ios", "android"],
        environment: "development",
        serviceAccountKey: {
          type: "",
          project_id: "",
          private_key_id: "",
          private_key: "",
          client_email: "",
          client_id: "",
          auth_uri: "",
          token_uri: "",
          auth_provider_x509_cert_url: "",
          client_x509_cert_url: "",
        },
      },
      */
    ],
    options: {
    },
    setExternal: (packages, options) => {
      if (Array.isArray(packages) && packages.length > 0) database.firebaseAdminExternal.projectPackages = packages;
      if (options) database.firebaseAdminExternal.options = options;
    },
  },
};

export default database;
