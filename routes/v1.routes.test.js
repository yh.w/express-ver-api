// route v1 integration tests

// Mocks functions
jest.mock("firebase-admin", () => {
  const getAuth = jest.fn();
  const auth = {
    mock: {
      getAuth,
    },
  };
  return { auth };
});
// jest.mock("../middlewares/externalProjectAuth.js", () => ({
//   verifyFirebaseIdToken: jest.fn((req, res, next) => { next(); }),
// }));
jest.mock("../config/database.js", () => ({
  firebaseAdmin: {
    init: jest.fn(),
    get: jest.fn(),
  },
  firebaseAdminExternal: {
    init: jest.fn(),
  },
}));
jest.mock("../middlewares/serverLogging.js", () => jest.fn((req, res, next) => { try { next(); } catch (ex) {} })); // Disable server logs
jest.mock("../utils/serverToken.js", () => {
  const original = jest.requireActual("../utils/serverToken.js");
  return {
    ...original,
    // Mock verify
    updateAndVerifyAccessToken: (req, forceRefresh) => {
      // route /auth should load real data
      if (req.path === "/auth") {
        return original.updateAndVerifyAccessToken(req, forceRefresh);
      }

      // Only accept dummyToken
      let token = req.headers["x-access-token"] || req.headers.authorization;
      const regex = /^Bearer\s+/;
      token = token && token.match(regex) ? token.replace(regex, "") : "";
      if ([dummyJwtIos, dummyJwtAndroid].indexOf(token) < 0) return "";

      // Simulate user data
      if (token === dummyJwtIos) user.platform = "ios";
      if (token === dummyJwtAndroid) user.platform = "android";
      user.token = {
        userId: user.uid,
        exp: Math.floor(new Date() / 1000) + 3600,
      };
      return user;
    },
  };
});
// Mock modules data
// jest.mock("../middlewares/moduleService.js", () => ({
//   ...jest.requireActual("../middlewares/moduleService.js"),
//   bundle: { getData: (db, route) => bundles() },
//   product: {
//     getData: (db, route) => {
//       const bundleProducts = bundles().filter(v => route.params?.bundle ? v.id === route.params.bundle : true); // filter if bundle id provided
//       return bundleProducts.map(b => {
//         b.products = products().filter(v => v.platform === route.user.platform && v.bundles.indexOf(b.id) >= 0); // Map products into bundles
//         return b;
//       });
//     }
//   },
//   entitlement: { getData: (db, route) => getEntitlement(db, route) },
//   iap: { getData: (db, route) => ([]) },
// }));
jest.mock("../utils/orm/firestoreOrm.js", () => ({
  ...jest.requireActual("../utils/orm/firestoreOrm.js"),
  upsertDocument: (db, req, result) => result,
}));

import "regenerator-runtime/runtime";
import supertest from "supertest";
import { app } from "../app.js";

const ver = "/v1";

// Dummy data
const user = {
  token: "dummyToken",
  uid: "jdfu0ptrGOhXtjlfaYKm",
  platform: "",
  package: "dummyPackageId",
  device: "dummyDeviceId",
  environment: "",
};
const dummyJwtIos = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6ImR1bW15VG9rZW4iLCJ1aWQiOiJqZGZ1MHB0ckdPaFh0amxmYVlLbSIsInBsYXRmb3JtIjoiaW9zIiwicGFja2FnZSI6ImR1bW15UGFja2FnZUlkIiwiZGV2aWNlIjoiZHVtbXlEZXZpY2VJZCIsImVudmlyb25tZW50IjoicHJvZHVjdGlvbiIsImlhdCI6MTYzOTE0NDQwNywiZXhwIjoxNjM5MjMwODA3fQ.MifDwL_RmzUTT0iOeGoXUqBpVNIOduKGSlK883Ce5Cw";
const dummyJwtAndroid = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6ImR1bW15VG9rZW4iLCJ1aWQiOiJqZGZ1MHB0ckdPaFh0amxmYVlLbSIsInBsYXRmb3JtIjoiaW9zIiwicGFja2FnZSI6ImR1bW15UGFja2FnZUlkIiwiZGV2aWNlIjoiZHVtbXlEZXZpY2VJZCIsImVudmlyb25tZW50IjoicHJvZHVjdGlvbiIsImlhdCI6MTYzOTExOTc5MSwiZXhwIjoxNjM5MjA2MTkxfQ.bFS5K8d74fRoEcB1WeA8zNqJ0vJNQ3r7lzFKYbZW53U";
const bundles = () => [
  {
    type: "PAID_SUBSCRIPTION",
    status: "active",
    createDatetime: {
      _seconds: 1637683200,
      _nanoseconds: 0
    },
    description: "Personalosed fitness programs and challenges\\nOver 100 AI-Powered workouts led by world-class trainers\\nAccess to global running clubs\\nGuided meditation\\nAutomatically renews by the end of each year",
    modifyDatetime: {
      _seconds: 1637683200,
      _nanoseconds: 0
    },
    modifiedBy: "",
    packageId: "",
    createdBy: "",
    name: "1 Year Plan",
    id: "1XGss7A7zJzqjuLKPSVM",
  },
  {
    createdBy: "",
    modifyDatetime: {
      _seconds: 1637856000,
      _nanoseconds: 0
    },
    status: "active",
    packageId: "",
    description: "Personalosed fitness programs and challenges\\nOver 100 AI-Powered workouts led by world-class trainers\\nAccess to global running clubs\\nGuided meditation\\nAutomatically renews by the end of each month",
    type: "PAID_SUBSCRIPTION",
    modifyBy: "",
    name: "1 Month Plan",
    createDatetime: {
      _seconds: 1637856000,
      _nanoseconds: 0
    },
    id: "9daHLuktY3JTFtZJbZ1x",
  },
];
const products = () => [
  {
    modifiedBy: "",
    createDatetime: {
      _seconds: 1637683200,
      _nanoseconds: 0
    },
    platform: "ios",
    createdBy: "",
    bundles: [
      "1XGss7A7zJzqjuLKPSVM"
    ],
    modifyDatetime: {
      _seconds: 1637683200,
      _nanoseconds: 0
    },
    sku: "yearlySubscription",
    status: "active",
    id: "IRQmBUMgpUNvLw58Su1a"
  },
  {
    createdBy: "",
    modifyBy: "",
    modifyDatetime: {
      _seconds: 1637856000,
      _nanoseconds: 0
    },
    platform: "ios",
    createDatetime: {
      _seconds: 1637856000,
      _nanoseconds: 0
    },
    status: "active",
    sku: "monthlySubscription",
    bundles: [
      "9daHLuktY3JTFtZJbZ1x"
    ],
    id: "d838XFhsuOEookibIFsC"
  },
  {
    bundles: [
      "9daHLuktY3JTFtZJbZ1x"
    ],
    status: "active",
    modifyDatetime: {
      _seconds: 1638288000,
      _nanoseconds: 0
    },
    createDatetime: {
      _seconds: 1638288000,
      _nanoseconds: 0
    },
    sku: "monthly.subscription",
    modifyBy: "",
    platform: "android",
    createdBy: "",
    id: "RkzNsizeDgILQbpPrPWi",
  },
  {
    sku: "yearly.subscription",
    bundles: [
      "1XGss7A7zJzqjuLKPSVM"
    ],
    status: "active",
    platform: "android",
    modifyDatetime: {
      _seconds: 1638288000,
      _nanoseconds: 0
    },
    modifyBy: "",
    createDateTime: {
      _seconds: 1638288000,
      _nanoseconds: 0
    },
    createdBy: "",
    id: "45CcruXyLhCnbeOrxV1C",
  }
];
// Entitlement receipts
const validReceiptBody = {
  status: "active",
  transactionId: "dummyTransaction",
  originalTransactionId: "dummyOriginalTransaction",
  verifiedTransaction: false,
  createdBy: "SYSYEM",
  modifiedBy: "SYSYEM",
  receiptBody: {
  },
  productSku: "dummyProduct",
  currency: "HKD",
  price: null,
  quantity: 0,
};
const invalidReceiptBody = {
  status: "inactive",
};
const getEntitlement = (db, route) => {
  const { platform } = route.user;
  const { transaction } = route.body;
  const receipt = !transaction ? "" : (platform === "android" ? transaction.purchaseToken : transaction.appStoreReceipt);
  return {
    ...(receipt === "dummyReceipt" ? validReceiptBody : invalidReceiptBody),
    platform,
    userId: route.body.id,
    sandbox: route.user.environment === "production",
    receipt,
    requestData: route.body,
  };
};

// Helper functions
const expect200 = res => {
  expect(res.statusCode).toEqual(200);
  const { body } = res;
  expect(body.status).toEqual("success");
};
const expect403 = res => {
  expect(res.statusCode).toEqual(403);
  const { body } = res;
  expect(body.status).toEqual("error");
  expect(body.data).toEqual(null);
  expect(body.message).toEqual("Invalid token");
};
const expect200Fail = res => {
  expect(res.statusCode).toEqual(200);
  const { body } = res;
  expect(body.status).toEqual("success");
  expect(body.message).toEqual(null);
};

afterAll(async () => {
  await new Promise(resolve => setTimeout(() => resolve(), 500)); // avoid jest open handle error
});

// Tests begin
describe("v1 endpoints", () => {
  it("(GET) /", async () => {
    const res = await supertest(app)
      .get(`${ver}`);
    expect(res.statusCode).toEqual(200);
    expect(res.text).toEqual("api v1");
  });
});

describe("(POST) /auth, grant access token on req body", () => {
  const postAuthTests = [
    {
      title: "Should return 403, empty platform & environment",
      user: () => {
        const userData = user;
        return userData;
      },
      expect: false,
    },
    {
      title: "Should return 403, empty platform",
      user: () => {
        const userData = user;
        userData.environment = "production";
        return userData;
      },
      expect: false,
    },
    {
      title: "Should return 200, empty environment",
      user: () => {
        const userData = user;
        userData.platform = "ios";
        return userData;
      },
      expect: true,
    },
    {
      title: "Should return 200, production, ios",
      user: () => {
        const userData = user;
        userData.platform = "ios";
        userData.environment = "production";
        return userData;
      },
      expect: true,
    },
    {
      title: "Should return 200, production, android",
      user: () => {
        const userData = user;
        userData.platform = "android";
        userData.environment = "production";
        return userData;
      },
      expect: true,
    },
    {
      title: "Should return 200, staging, ios",
      user: () => {
        const userData = user;
        userData.platform = "ios";
        userData.environment = "staging";
        return userData;
      },
      expect: true,
    },
    {
      title: "Should return 200, staging, android",
      user: () => {
        const userData = user;
        userData.platform = "android";
        userData.environment = "staging";
        return userData;
      },
      expect: true,
    },
    {
      title: "Should return 200, development, ios",
      user: () => {
        const userData = user;
        userData.platform = "ios";
        userData.environment = "development";
        return userData;
      },
      expect: true,
    },
    {
      title: "Should return 200, development, android",
      user: () => {
        const userData = user;
        userData.platform = "android";
        userData.environment = "development";
        return userData;
      },
      expect: true,
    },
    {
      title: "Should return 403, empty req body",
      user: () => ({}),
      expect: false,
    },
    {
      title: "Should return 403, single body field, token",
      user: () => ({ token: "dummy" }),
      expect: false,
    },
    {
      title: "Should return 403, single body field, uid",
      user: () => ({ uid: "dummy" }),
      expect: false,
    },
    {
      title: "Should return 403, single body field, package",
      user: () => ({ package: "dummy" }),
      expect: false,
    },
    {
      title: "Should return 403, single body field, device",
      user: () => ({ device: "dummy" }),
      expect: false,
    },
    {
      title: "Should return 403, missing field, token",
      user: () => {
        const userData = user;
        userData.platform = "ios";
        delete userData.token;
        return userData;
      },
      expect: false,
    },
    {
      title: "Should return 403, missing field, uid",
      user: () => {
        const userData = user;
        userData.platform = "ios";
        delete userData.uid;
        return userData;
      },
      expect: false,
    },
    {
      title: "Should return 403, missing field, package",
      user: () => {
        const userData = user;
        userData.platform = "ios";
        delete userData.package;
        return userData;
      },
      expect: false,
    },
    {
      title: "Should return 403, missing field, device",
      user: () => {
        const userData = user;
        userData.platform = "ios";
        delete userData.device;
        return userData;
      },
      expect: false,
    },
  ];
  postAuthTests.forEach(v => {
    it(v.title, async () => {
      const userData = v.user();
      const res = await supertest(app)
        .post(`${ver}/auth`)
        .set({ "Content-Type": "application/json" })
        .send(userData);

      if (v.expect) {
        // on success
        expect200(res);
        const { body } = res;
        expect(body.data).toHaveProperty("user");
      } else {
        // on failed
        expect403(res);
      }
    });
  });
});
describe("(POST) /auth, return user data with Authorization header", () => {
});

// Univerisal tests on Authorization header
const authHeaderTests = [
  {
    title: "Should return 403, empty Authorization header",
    bearer: () => "",
    expect: false,
  },
  {
    title: "Should return 403, empty Authorization header prefix, ios",
    bearer: () => dummyJwtIos,
    expect: false,
  },
  {
    title: "Should return 403, empty Authorization header prefix, android",
    bearer: () => dummyJwtAndroid,
    expect: false,
  },
  {
    title: "Should return 403, invalid Authorization header prefix, ios",
    bearer: () => `Auth ${dummyJwtIos}`,
    expect: false,
  },
  {
    title: "Should return 403, invalid Authorization header prefix, android",
    bearer: () => `Auth ${dummyJwtAndroid}`,
    expect: false,
  },
  {
    title: "Should return 403, invalid Authorization header, ios",
    bearer: () => `Bearer ${dummyJwtIos}123`,
    expect: false,
  },
  {
    title: "Should return 403, invalid Authorization header, android",
    bearer: () => `Bearer ${dummyJwtAndroid}123`,
    expect: false,
  },
  {
    title: "Should return 200, valid Authorization header, ios",
    bearer: () => `Bearer ${dummyJwtIos}`,
    platform: "ios",
    expect: true,
  },
  {
    title: "Should return 200, valid Authorization header, android",
    bearer: () => `Bearer ${dummyJwtAndroid}`,
    platform: "android",
    expect: true,
  },
];

describe("(GET) /bundles, get bundles only", () => {
  const getBundlesTests = authHeaderTests;
  getBundlesTests.forEach(v => {
    it(v.title, async () => {
      const res = await supertest(app)
        .get(`${ver}/bundles`)
        .set({ "Content-Type": "application/json", Authorization: v.bearer ? v.bearer() : `Bearer ${dummyJwt}` });

      if (v.expect) {
        // on success
        expect200(res);
        const { body } = res;
        expect(body.data.length).toEqual(bundles().length);
        expect(body.data.data.length).toEqual(bundles().length);
      } else {
        // on failed
        expect403(res);
      }
    });
  });
});

describe("(GET) /products, get bundles with products included", () => {
  const getBundlesTests = authHeaderTests;
  getBundlesTests.forEach(v => {
    it(v.title, async () => {
      const res = await supertest(app)
        .get(`${ver}/products`)
        .set({ "Content-Type": "application/json", Authorization: v.bearer ? v.bearer() : `Bearer ${dummyJwt}` });

      if (v.expect) {
        // on success
        expect200(res);
        const { body } = res;
        expect(body.data.length).toEqual(bundles().length);
        expect(body.data.data.length).toEqual(bundles().length);
        body.data.data.forEach(bundle => {
          expect(bundle.products.filter(p => p.platform === v.platform).length).toEqual(bundle.products.length);
        });
      } else {
        // on failed
        expect403(res);
      }
    });
  });
});

describe("(GET) /products/:bundle, get filtered bundle with products included", () => {
  const bundleId = "9daHLuktY3JTFtZJbZ1x";
  const getBundlesTests = authHeaderTests;
  getBundlesTests.forEach(v => {
    it(v.title, async () => {
      const res = await supertest(app)
        .get(`${ver}/products/${bundleId}`)
        .set({ "Content-Type": "application/json", Authorization: v.bearer ? v.bearer() : `Bearer ${dummyJwt}` });

      if (v.expect) {
        // on success
        expect200(res);
        const { body } = res;
        body.data.data.forEach(bundle => {
          if (bundle.id === bundleId) {
            expect(bundle.products.filter(p => p.platform === v.platform).length).toEqual(bundle.products.length);
          }
        });
      } else {
        // on failed
        expect403(res);
      }
    });
  });
});
/*
// TODO
describe("(GET) /product/:id, get product by id", () => {
    it(v.title, async () => {
      const res = await supertest(app)
        .get(`${ver}/product/:id`);
    });
});
*/
/*
describe("(GET) /user, get user details", () => {
    it(v.title, async () => {
      const res = await supertest(app)
        .get(`${ver}/user`);
    });
});
*/
describe("(POST) /iap/purchase, grant access token on req body", () => {
  // Authorization tests
  const iapPurchaseAuthTests = authHeaderTests.filter(v => !v.expect);
  iapPurchaseAuthTests.forEach(v => {
    it(v.title, async () => {
      const res = await supertest(app)
        .post(`${ver}/iap/purchase`)
        .set({ "Content-Type": "application/json", Authorization: v.bearer ? v.bearer() : `Bearer ${dummyJwt}` });
      expect403(res);
    });
  });

  // Mock axios
  const iapReceipts = [
    {
      title: "Should return false status, on empty receipt",
      expect: false,
      body: {},
    },
    {
      title: "Should return false status, on invalid receipt",
      expect: false,
      body: {
        id: "123123",
        transaction: {
          appStoreReceipt: "123",
        },
      },
    },
    {
      title: "Should return false status, on dummy receipt",
      expect: true,
      body: {
        id: "123123",
        transaction: {
          appStoreReceipt: "dummyReceipt",
        },
      },
    },
  ];
  const iapReceiptAuths = authHeaderTests.filter(v => v.expect && v.platform === "ios"); // TODO ios first
  iapReceipts.forEach(v => {
    it(v.title, async () => {
      iapReceiptAuths.forEach(async p => {
        const res = await supertest(app)
          .post(`${ver}/iap/purchase`)
          .set({ "Content-Type": "application/json", Authorization: p.bearer ? p.bearer() : `Bearer ${dummyJwt}` })
          .send(v.body);

        if (v.expect) {
          // on success
          expect200(res);
          const body = res.body.data;
          expect(body.status).toEqual("active");
        } else {
          // on failed
          expect200Fail(res);
        }
      });
    });
  });
});
/*
describe("(POST) /iap/receipt/:platform, grant access token on req body", () => {
    it(v.title, async () => {
      const res = await supertest(app)
        .post(`${ver}/purchase`);
    });
});
*/
