import express from "express";
import routerBuilder from "../utils/routerBuilder.js";
import serverLogging from "../middlewares/serverLogging.js";
import authorization from "../middlewares/authorization.js";
// Modules per features
import moduleFrame from "../modules/moduleFrameIndex.js"; // A base example
import userModule from "../modules/user/index.js";
import userMetaModule from "../modules/user/user_meta.js";
import userProfileModule from "../modules/user/user_profile.js";
import tradeModule from "../modules/trade/index.js";
import redeemModule from "../modules/redeem/index.js";

const router = express.Router();
// Init builder
const builder = routerBuilder(router);

const routes = (options = null) => {
  // Default options
  if (!options) {
    options = process.env.NODE_ENV === "localhost"
      ? { // Enable all modules on localhost
        auth: true,
        user: true,
        userProfile: true,
        trade: true,
        redeem: true,
      }
      : {
        auth: true, // Base endpoints
        user: true, // Base endpoints
        userProfile: false,
        trade: false,
        redeem: false,
      };
  }

  router.get("/", (req, res) => {
    console.log("Request api v1");
    res.send("api v1");
  });

  // Authorization
  router.post("/auth", serverLogging.request, authorization.verifyAndAccessTokenGateway, authorization.authorizated, serverLogging.response);
  // Get details and refresh token
  router.get("/auth", serverLogging.request, authorization.verifyAndAccessTokenGateway, authorization.authorizated, serverLogging.response);

  // Modules
  if (process.env.NODE_ENV === "localhost") moduleFrame.init(builder); // Only showm base example on development
  if (options.auth === true) userModule.init(builder);
  if (options.user === true) userMetaModule.init(builder);
  if (options.userProfile === true) userProfileModule.init(builder);
  if (options.trade === true) tradeModule.init(builder);
  if (options.redeem === true) redeemModule.init(builder);

  return router;
};

export default routes;
