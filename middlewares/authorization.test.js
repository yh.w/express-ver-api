import "regenerator-runtime/runtime";
import authorization, { authFields } from "./authorization.js";
import * as serverToken from "../utils/serverToken.js";

const updateAndVerifyAccessTokenRef = 
  jest.spyOn(serverToken, "verifyAndAccessTokenGateway")
  .mockImplementation(jest.fn(req => (req ? { ok: true } : null)));

afterEach(() => {
  [
    updateAndVerifyAccessTokenRef,
    console.log,
    next,
  ].map(v => v.mockClear());
});

const dummyData = { ok: true };
const req = {};
req[authFields.reqField] = {
  user: dummyData,
};
const status = jest.fn(() => json);
const json = { json: jest.fn() };
const res = { status };
console.log = jest.fn();
const next = jest.fn();

describe("authorization functions", () => {
  it("authorizated(), should trigger response", () => {
    // authorization.authorizated(req, res, next);
    // expect(status).toBeCalled();
    // expect(json.json).toBeCalled();
    // expect(console.log).toBeCalled();
    // expect(next).not.toBeCalled();
  });
  it("authorizated(), should process next(), on valid user", () => {
    // authorization.verifyAndAccessTokenGateway(req, res, next);
    // expect(updateAndVerifyAccessTokenRef).toBeCalled();
    // expect(req[authFields.reqField].user).toEqual(dummyData);
    // expect(next).toBeCalled();
  });
  it("authorizated(), should trigger response, on empty user", () => {
    // authorization.verifyAndAccessTokenGateway(null, res, next);
    // expect(updateAndVerifyAccessTokenRef).toBeCalled();
    // expect(status).toBeCalled();
    // expect(json.json).toBeCalled();
    // expect(console.log).toBeCalled();
    // expect(next).not.toBeCalled();
  });
});
