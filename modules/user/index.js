import { path, routeInsert, routeUpdate, routeDeleteOrSoft } from "../moduleFrame.js";
import userService from "./services/user.service.js";
import authService from "../auth/services/auth.service.js";
import resultObject, { stateConstant } from "../../utils/resultObject.js";

const modulePath = "/user";

const getUsers = async (req, res) => {
  userService.init(req);

  const ids = req.params.userId;
  let where = { field: "id", condition: "==" };
  where.value = ids;

  // Search with target
  if (typeof where.value === "string") where.condition = "==";
  else if (typeof where.value === "object" && where.value.length > 0) where.condition = "in";

  // Search active only
  if (!where.value) {
    where = {
      field: "status",
      condition: "==",
      value: "active",
    };
  };

  const result = await userService.get(where);
  res.send(result);
};

const insertUser = async (req, res) => {
  const result = await routeInsert(req, res, userService);
  res.send(result);
};

const updateUser = async (req, res) => {
  const result = await routeUpdate(req, res, userService);
  res.send(result);
};

const deleteUser = async (req, res) => {
  req.params.id = req.params.userId;
  const result = await routeDeleteOrSoft(req, res, userService);
  res.send(result);
};

const syncNewUser = async (req, res) => {
  userService.init(req);
  let result = resultObject(false, stateConstant.ERROR_DATA_INVALID);
  const { data } = req.body;

  if (data) {
    // Split essential fields
    const user = {};
    const userProfiles = {};
    const userFields = {
      required: [ // essential data
        "provider",
      ],
      optional: [
        "authUid",
        "package",
        "email",
        "username",
        "emailVerified",
        "password",
      ],
    };
    const userMetaFields = [
      // User model data
      "createDatetime",
      "status",
      "user_meta",
    ];
    Object.entries(data).map(([k, v]) => {
      if ([...userFields.required, ...userFields.optional, ...userMetaFields].indexOf(k) >= 0) user[k] = v;
      else userProfiles[k] = v;
    });

    // Create user
    user.provider = authService.validateProvider(user.provider, user); // Map provider id to config values
    const insertData = {
      ...user,
      user_meta: { ...user.user_meta, ...userProfiles },
    };
    if (
      userFields.required.filter(v => user[v]).length === userFields.required.length
      && user.provider
    ) { // Check user values not null
      result = await userService.syncNewUser(insertData);
    };
  }

  res.send(result);
};

const module = {
  init: builder => {
    builder("get", path("", modulePath), getUsers, true);
    builder("get", path("/:userId", modulePath), getUsers, true);
    builder("post", path("", modulePath), insertUser, true);
    builder("post", path("/:userId", modulePath), insertUser, true);
    builder("patch", path("", modulePath), updateUser, true);
    builder("patch", path("/:userId", modulePath), updateUser, true);
    builder("delete", path("/:userId", modulePath), deleteUser, true);

    // PUT method, reserve for external features, build on existing endpoints
    builder("put", path("/sync", modulePath), syncNewUser, false); // Create user on air
  },
};

export default module;
