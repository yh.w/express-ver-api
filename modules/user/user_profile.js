import { path } from "../moduleFrame.js";
import userProfileService from "./services/user_profile.service.js";
import resultObject, { stateConstant } from "../../utils/resultObject.js";
import authData from "../../utils/authData.js";

const modulePath = "/userProfile";

const getUserProfile = async (req, res) => {
  userProfileService.init(req);
  const tokenUser = authData(req);
  if (!tokenUser || !tokenUser?.user?.id) {
    res.send(resultObject(false, stateConstant.ERROR_DATA_NOT_FOUND));
    return;
  }
  const { id } = req.params;
  const result = await userProfileService.getUserProfile(tokenUser, id);
  res.send(result);
};

const createUserProfile = async (req, res) => {
  userProfileService.init(req);
  const tokenUser = authData(req);
  if (!tokenUser || !tokenUser?.user?.id) {
    res.send(resultObject(false, stateConstant.ERROR_DATA_NOT_FOUND));
    return;
  }
  const { data } = req.body;
  const result = await userProfileService.createUserProfile(tokenUser, data);
  res.send(result);
};

const updateUserProfile = async (req, res) => {
  userProfileService.init(req);
  const tokenUser = authData(req);
  if (!tokenUser || !tokenUser?.user?.id) {
    res.send(resultObject(false, stateConstant.ERROR_DATA_NOT_FOUND));
    return;
  }
  const { id } = req.params;
  const { data } = req.body;
  const result = await userProfileService.updateUserProfile(tokenUser, { id, ...data });
  res.send(result);
};

const deleteUserProfile = async (req, res) => {
  userProfileService.init(req);
  const tokenUser = authData(req);
  if (!tokenUser || !tokenUser?.user?.id) {
    res.send(resultObject(false, stateConstant.ERROR_DATA_NOT_FOUND));
    return;
  }
  const { id } = req.params;
  const result = await userProfileService.deleteUserProfile(tokenUser, id);
  res.send(result);
};

const module = {
  init: builder => {
    builder("get", path("", modulePath), getUserProfile, true);
    builder("get", path("/:id", modulePath), getUserProfile, true);
    builder("post", path("", modulePath), createUserProfile, true);
    builder("put", path("/:id", modulePath), updateUserProfile, true);
    builder("delete", path("/:id", modulePath), deleteUserProfile, true);
  },
};

export default module;
