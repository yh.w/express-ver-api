import { path } from "../moduleFrame.js";
import userMetaService from "./services/user_meta.service.js";
import resultObject, { stateConstant } from "../../utils/resultObject.js";
import authData from "../../utils/authData.js";

const modulePath = "/userDetails";

const getUserMeta = async (req, res) => {
  userMetaService.init(req);
  const tokenUser = authData(req);
  if (!tokenUser) {
    res.send(resultObject(false, stateConstant.ERROR_DATA_NOT_FOUND));
    return;
  }
  const result = await userMetaService.getUserMeta(tokenUser);
  res.send(result);
};

const module = {
  init: builder => {
    builder("get", path("", modulePath), getUserMeta, true);
  },
};

export default module;
