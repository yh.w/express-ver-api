import admin from "firebase-admin";
import database from "../../../config/database.js";
import userModel, { validPassword } from "../models/user.model.js";
import resultObject, { stateConstant } from "../../../utils/resultObject.js";
import callbacksUtils from "../../../utils/callbacksUtils.js";

const mergeUserProviders = false;

const getUsers = async (queries, options = {}) => {
  const data = await userModel.get({
    where: queries,
    orderBy: queries?.orderBy ? queries.orderBy : (!queries?.orderBy ? {} : { field: queries ? queries.field : "createDatetime", sort: "asc" }),
  }, options);
  return data;
};

const upsertUser = async (data, options = {}) => {
  // Prepare data
  const id = data.id;
  if (!id) {
    // Delete inproper
    delete data._id;
    delete data.id;
  }

  if (options?.checkExist) {
    if (id) {
      // Only on update
      const data = await service.getUsers({
        field: "id",
          condition: "==",
        value: id,
      });

      if (data?.status === "success" && data?.data.length !== 1) {
        return resultObject(false, stateConstant.ERROR_DATA_INVALID);
      }
    }
  }

  const result = await userModel.upsert(data, options);
  return result;
};

const deleteUser = async ids => {
  const data = { id: ids, status: "deleted" };
  const user = await userModel.get({
    where: {
      field: "id",
      condition: "==",
      value: ids,
    },
    //orderBy,
  });
  if (user.status !== "success" || user.data.length < 1) {
    res.send(resultObject(false, stateConstant.ERROR_FORBIDDEN));
    return;
  }

  const result = await userModel.upsert(data, { update: true });
  return result;
};

const service = {
  init: (req) => {
    userModel.init(req);
  },
  get: getUsers,
  upsert: upsertUser,
  delete: deleteUser,
  // For auth
  validPassword,
  syncNewUser: async data => {
    // Create user data
    let newUserData = {
      ...data,
      email: data.email || "",
      username: data.username || "",
      emailVerified: data.emailVerified || false,
      oauth: [data.provider],
      authUid: data.authUid ? [data.authUid] : [],
      createDatetime: data.creationTime || new Date(),
    };
    newUserData = await callbacksUtils.fetch(callbacks, "beforeFilter", { state: "syncNewUser", data: newUserData });
    let result = resultObject(false, stateConstant.ERROR_DATA_INVALID);
    if (!newUserData) return result;

    // Search existing user
    let searchUser = {};
    if (["email", "username"].indexOf(newUserData.provider) >= 0) {
      // Search by fields
      if (newUserData.email) {
        searchUser = await service.get({
          field: "email",
          condition: "==",
          value: newUserData.email,
          orderBy: false,
        });
      }
      if (newUserData.username) {
        searchUser = await service.get({
          field: "username",
          condition: "==",
          value: newUserData.username,
          orderBy: false,
        });
      }
    } else {
      // Search by authUid, from external idToken
      try {
        const authToken = await admin.auth().verifyIdToken(data.token);
        const { uid, userId, firebase } = authToken;
        const { sign_in_provider } = firebase;
        if (uid) newUserData.authUid = [uid]; // uid, firebase auth uuid
        if (sign_in_provider) newUserData.provider = sign_in_provider; // provider from verified token
        else newUserData = false;
      } catch (ex) {
        // FIXME Assume no verify on incoming traffic
        // if (process.env.NODE_ENV === "localhost") { // By-pass user auth when localhost
          if (newUserData) {
            if (!newUserData.authUid) newUserData.authUid = newUserData.authUid || [Date.now()];
            if (!newUserData.provider) newUserData.provider = "google.com";
          }
        // }
        else newUserData = false;
      }
    }

    if (searchUser.data?.length > 1) {
      return resultObject(newUserData, stateConstant.ERROR_DATA_DUPLICATED);
    }

    // TODO disabled temporary
    // Abort merge if multple users found
    // Merge existing 1 user provider
    if (searchUser.data?.length === 1) {
      if (mergeUserProviders) {
        const userData = await searchUser.data[0];

        // Update fields
        userData.updateDatetime = new Date();
        // See user.model for definition
        if (!userData.oauth?.length) userData.oauth = [];
        userData.oauth.push(newUserData.oauth[0]);
        // See user.model for definition
        if (!userData.authUid?.length) userData.authUid = [];
        userData.authUid.push(newUserData.authUid[0]);

        result = await service.upsertUser(userData);
        if (result.status === "error") return resultObject(newUserData, result.message);
        return resultObject(userData, false, `Merged user[${userData.id}], new provider: ${newUserData.oauth[0]}`);
      } else {
        return resultObject(newUserData, stateConstant.ERROR_DATA_DUPLICATED);
      }
    }

    // Create new user if not found then...

    // Create new user
    result = await service.upsert(newUserData, true);

    result.data = await callbacksUtils.fetch(callbacks, "afterFilter", { state: "syncNewUser", data: result.data });

    // Send verification if needed
    if (data.provider === "password") {
      service.sendEmailVerification(newUserData);
    }

    return result;
  },
  sendEmailVerification: async (authUser) => {
    if (!authUser.emailVerified && authUser.authUid.length > 0) {
      // Send email
      try {
        const customToken = await admin.auth().createCustomToken(authUser.authUid[0]);

        const { idToken } = await fetch(exchangeCustomTokenEndpoint, {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            token: customToken,
            returnSecureToken: true,
          }),
        }).then((res) => res.json());

        const response = await fetch(sendEmailVerificationEndpoint, {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            requestType: "VERIFY_EMAIL",
            idToken: idToken,
          }),
        }).then((res) => res.json());
        console.log(response);

        // Sent email
      } catch (error) {
        // Failed
        console.log(error);
      }
    }
  },
};

export const callbacks = {
  beforeFilter: (func = "", data) => data,
  afterFilter: (func = "", data) => data,
};

export default service;
