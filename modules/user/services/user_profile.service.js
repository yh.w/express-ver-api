import userProfileModel from "../models/user_profile.model.js";
import callbacksUtils from "../../../utils/callbacksUtils.js";

// get user profile
// if profileId is provided, retrieve only the specified profile
// else, retrieve all user profiles by default
const getUserProfile = async (tokenUser, profileId = null) => {
  let theUser = tokenUser;

  theUser = await callbacksUtils.fetch(callbacks, "beforeFilter", { state: "getUserProfile", data: theUser });

  const id = theUser.user.id;
  const where = { field: "userId", condition: "==", value: id };
  if (profileId) {
    where.field = "id";
    where.value = profileId;
  };

  const result = await userProfileModel.get({
    where
  }, { activeOnly: true });

  result.data = await callbacksUtils.fetch(callbacks, "afterFilter", { state: "getUserProfile", data: result.data });

  return result;
};

const createUserProfile = async (tokenUser, profileData) => {
  let theUser = tokenUser;

  theUser = await callbacksUtils.fetch(callbacks, "beforeFilter", { state: "createUserProfile", data: theUser });

  const { user } = theUser;

  const options = {
    activeOnly: true
  };

  if (profileData?.displayProfileId) options.unique = { field: "displayProfileId" };

  const result = await userProfileModel.upsert({
    userId: user.id,
    ...profileData
  }, options);

  result.data = await callbacksUtils.fetch(callbacks, "afterFilter", { state: "createUserProfile", data: result.data });
  return result;
};

const deleteUserProfile = async (tokenUser, id) => {
  let theUser = tokenUser;

  theUser = await callbacksUtils.fetch(callbacks, "beforeFilter", { state: "deleteUserProfile", data: theUser });

  const { user } = theUser;
  const result = await userProfileModel.upsert({
    userId: user.id,
    id,
    status: "deleted"
  }, {
    updateOnly: true
  });

  return result;
};

const updateUserProfile = async (tokenUser, data) => {
  let theUser = tokenUser;

  theUser = await callbacksUtils.fetch(callbacks, "beforeFilter", { state: "updateUserProfile", data: theUser });

  const options = {
    updateOnly: true,
    activeOnly: true
  };

  const { user } = theUser;
  const result = await userProfileModel.upsert({
    userId: user.id,
    ...data,
  }, options);

  return result;
};

const service = {
  init: req => {
    userProfileModel.init(req);
  },
  getUserProfile,
  createUserProfile,
  updateUserProfile,
  deleteUserProfile
};

export const callbacks = {
  beforeFilter: (func = "", data) => data,
  afterFilter: (func = "", data) => data,
};

export default service;
