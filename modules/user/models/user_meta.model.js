import mongoose from "mongoose";
import modelUtils from "../../../utils/modelUtils.js";

const { Schema } = mongoose;

export const collection = "user_meta";

export const schema = {
  userId: {
    type: String,
    required: true,
  },
  firstname: {
    type: String,
    default: "",
  },
  lastname: {
    type: String,
    default: "",
  },
  nickname: {
    type: String,
    default: "User",
  },
  meta: {
    type: Schema.Types.Mixed,
    default: {},
  },
};

const dataSource = "firestore";

export default modelUtils({
  dataSource,
  collection,
  schema,
});
