import crypto from "crypto";
import userMetaModel from "./user_meta.model.js";
import { getProviders } from "../../auth/models/auth.model.js";
import modelUtils from "../../../utils/modelUtils.js";

export const collection = "users";

export const schema = {
  email: {
    type: String,
    unique: true,
  },
  username: {
    type: String,
    unique: true,
  },
  // For oauth, indicate platforms, should equal to number of enum values only
  oauth: [{
    type: String,
    enum: {
      values: getProviders(),
      message: "{VALUE} is not supported",
    },
  }],
  // For oauth, indicate oauth credential, can have multiple (e.g. different projects) for same platforms
  authUid: [{
    type: String,
  }],
  isEmailVerified: {
    type: Boolean,
  },
  // For password
  salt: {
    type: String,
  },
  // For password
  hash: {
    type: String,
  },
};

const generateSaltHash = password => {
  const data = {};
  // Creating a unique salt for a particular user
  data.salt = crypto.randomBytes(16).toString("hex");
  // Hashing user's salt and password with 1000 iterations,
  data.hash = crypto.pbkdf2Sync(password, data.salt, 1000, 64, "sha512").toString("hex");
  return data;
};

export const validPassword = (validate, password) => {
  const hash = crypto.pbkdf2Sync(password, validate.salt, 1000, 64, "sha512").toString("hex");
  return validate.hash === hash;
};

export const removeSensitive = data => {
  if (!data) return data;
  delete data.salt;
  delete data.hash;
  delete data.oauth;
  delete data.authUid;
  return data;
};

const events = {
  onPackData: data => {
    if (data.password) {
      const saltHash = generateSaltHash(data.password);
      data = { ...data, ...saltHash };
      delete data.password;
    }
    if (data.oauth) {
      data.oauth = [...new Set(data.oauth)];
    }

    return data;
  },
  afterRetrive: data => {
    const newData = removeSensitive(data);
    return newData;
  },
};

const associations = {
  user_meta: {
    foreignKey: "userId",
    model: userMetaModel,
    fields: [
      "firstname",
      "lastname",
      "nickname",
      "settings",
    ],
  },
};

const viewSuffix = "users";

const dataSource = "firestore";

export default modelUtils({
  events,
  associations,
  dataSource,
  collection,
  viewSuffix,
  schema,
});
