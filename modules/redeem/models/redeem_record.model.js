import mongoose from "mongoose";
import modelUtils, { validator } from "../../../utils/modelUtils.js";

const { Schema } = mongoose;

export const collection = "redeem_records";

export const schema = {
  ownerId: { // Relation to external id, namly owner
    type: String,
  },
  codeId: { // Relation to code id
    type: String,
  },
  code: { // As snapshot code string
    type: String,
  },
  redeemDatetime: {
    type: Date,
    required: true,
  },
  platform: {
    type: String,
  },
};
const events = false;

const associations = false;

const viewSuffix = false;

const dataSource = "firestore";

export default modelUtils({
  events,
  associations,
  dataSource,
  collection,
  viewSuffix,
  schema,
});
