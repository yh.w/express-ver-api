import mongoose from "mongoose";
import axios from "axios";
import redeemCodeModel, { schema as redeemCodeSchema } from "../models/redeem_code.model.js";
import redeemRecordModel from "../models/redeem_record.model.js";
import resultObject, { stateConstant } from "../../../utils/resultObject.js";
import callbacksUtils from "../../../utils/callbacksUtils.js";

const { Schema } = mongoose;

const codeValidator = (doc, params = {}) => {
  const { today, counts, limit, platform, region } = params;
  const start = doc.startDatetime ? doc.startDatetime.toDate() : null;
  const end = doc.endDatetime ? doc.endDatetime.toDate() : null;

  return (
    doc.status === "active"
    && ((start && start <= today) || !start)
    && ((end && end >= today) || !end)
    && ((doc.redeemCounts && doc.redeemCounts > counts) || !counts)
    && ((doc.ownerRedeemLimits && doc.ownerRedeemLimits > limit) || !limit)
    && ((platform && doc.platforms.indexOf(platform) >= 0) || !platform)
    && ((region && doc.regionsIncluded.indexOf(region) >= 0) || !region)
    && ((region && doc.regionsExcluded.indexOf(region) < 0) || !region)
  );
};

const insert = async body => {
  if (!body.data) {
    return resultObject(false, stateConstant.ERROR_DATA_INVALID);
  }

  // Pack data
  let data = body.data;
  data.startDatetime = data.startDatetime ? new Date(data.startDatetime) : null;
  data.endDatetime = data.endDatetime ? new Date(data.endDatetime) : null;
  data.regionsIncluded = Array.isArray(data.regionsIncluded) ? data.regionsIncluded : [];
  data.regionsExcluded = Array.isArray(data.regionsExcluded) ? data.regionsExcluded : [];
  data = {
    ...data, // Pack data value
    redeemCounts: 0,
    status: "active",
  };

  // Validate data
  const Model = mongoose.model("redeemCodeModel", new Schema(redeemCodeSchema, { _id: false }));
  const object = new Model(data);
  const validation = object.validateSync();
  if (validation) {
    return resultObject(false, stateConstant.ERROR_DATA_INVALID);
  }
  data = object.toObject();

  // Check unique code
  const codes = await redeemCodeModel.get({ where: { field: "code", condition: "==", value: data.code }});
  if (!codes.data || codes.data.length > 0) {
    return resultObject(false, stateConstant.ERROR_DATA_INVALID);
  }

  // Only work on local atm
  if (process.env.NODE_ENV === "localhost") {
    const result = await redeemCodeModel.upsert(data);
    if (result && result._path?.segments[1]) data.id = result._path.segments[1];
  }
  return resultObject({ data });
};

const redeemStatus = {
  NOT_FOUND: "CODE_NOT_FOUND",
  INVALID: (val = "DATA") => `INVALID_${val.toUpperCase()}`,
};

const redeem = async params => {
  let result = { redeemed: false };
  const { code = "", origins = {} } = params;

  // Default values
  if (!origins.platform) origins.platform = "web";
  if (!origins.environment) origins.environment = "production";

  // Mandatory fields
  if (!origins.package || !origins.owner?.id) return result;

  // Validations
  let isValidRedemption = true;
  let message = ""; // For debug message

  // Get codes
  const codes = await redeemCodeModel.get({ where: { field: "code", condition: "==", value: code } });
  if (codes.data.length < 1) {
    message = redeemStatus.NOT_FOUND;
    isValidRedemption = false;
  }
  const codeData = codes.data.length > 0 ? codes.data[0] : null;
  if (!codeData) isValidRedemption = false;

  // Check platform
  if (isValidRedemption && codeData.platforms.indexOf(origins.platform) < 0) {
    message = redeemStatus.INVALID("platform");
    isValidRedemption = false;
  }

  // Check regions, with ip & country codes
  if (isValidRedemption && (codeData.regionsIncluded.length > 0 | codeData.regionsExcluded.length > 0)) {
    // Get country code from ip
    const ip = origins.ip;
    const ipapi = (await axios.get(`https://ipapi.co/${ip}/json`)).data || {};
    const countryCode = ipapi.country_code_iso3;
    if (
      !ip || !countryCode
      || (
        (codeData.regionsIncluded.length > 0 && codeData.regionsIncluded.indexOf(countryCode) < 0)
        || (codeData.regionsExcluded.length > 0 && codeData.regionsExcluded.indexOf(countryCode) > 0)
      )
    ) {
      message = redeemStatus.INVALID("regions");
      isValidRedemption = false;
    }
  }

  // Check owner redeem count
  if (isValidRedemption && codeData.redeemLimits > 0 && codeData.redeemLimits < codeData.redeemCounts) {
    message = redeemStatus.INVALID("redeem_limit");
    isValidRedemption = false;
  }

  // Count owner
  if (isValidRedemption && codeData.ownerRedeemLimits > 0) {
    const owners = (await redeemRecordModel.get({ where: { field: "ownerId", condition: "==", value: origins.owner.id } }))
      .data
      .filter(v =>
        v.codeId === codeData.id
        && v.status === "active"
      )
    ;
    if (owners.length >= codeData.ownerRedeemLimits) {
      message = redeemStatus.INVALID("count_limit");
      isValidRedemption = false;
    }
  }

  isValidRedemption = await callbacksUtils.fetch(callbacks, "beforeFilter", { state: "redeem", data: { isValidRedemption, code: codeData, origins } });

  if (isValidRedemption) {
    if (codeData.type === "custom_data") {
      // Parse meta field
      result = { data: {} };
      if ([codeData, codeData?.meta, codeData?.meta?.custom_data].filter(v => Object.keys(v).length > 0).length > 0) { // Check field `meta.custom_data` exist
        result = codeData.meta.custom_data[origins.platform] || codeData.meta.custom_data; // Use platform string key or whole object if not found
      }

      result = resultObject(result, false, "type:custom_data");
    } else {
      // Insert redeem record
      result = {
        ownerId: origins.owner.id,
        codeId: codeData.id,
        code,
        redeemDatetime: new Date(),
        platform: origins.platform,
      };
      result = (await redeemRecordModel.upsert(result)).data;

      // Update code counts
      if (result) {
        codeData.redeemCounts += 1;
        redeemCodeModel.upsert(codeData, { update: true }); // Do in sync
      }
    }
  }

  result = await callbacksUtils.fetch(callbacks, "afterFilter", { state: "redeem", data: result });

  return resultObject(
    result,
    false,
    message
  );
};

const authorizated = async origins => {
  const { owner: { id: ownerId }, platform, package: bundleId, device, environment } = origins;
  const data = {
  };
  const today = new Date();

  // Get valid promo codes
  data.isPromo = false;
  const codeDocs = (await redeemCodeModel.get({ where: { field: "status", condition: "==", value: "active" } })).data;
  const codeIds = [];
  const codes = {};
  codeDocs.forEach(doc => {
    if (codeValidator(doc, {
      platform,
      today,
    })) {
      // Data type validations
      if (doc.availablePeriod && (typeof doc.availablePeriod) === "string") doc.availablePeriod = parseInt(doc.availablePeriod);

      codeIds.push(doc.id);
      codes[doc.id] = doc;
    }
  });
  if (codeIds.length > 0) {
    const ownerDocs = (await redeemRecordModel.get({ where: { field: "ownerId", condition: "==", value: ownerId } })).data;
    const ownerCodes = ownerDocs
      .filter(v =>
        v.status === "active"
        && v.platform === platform
        && codeIds.indexOf(v.codeId) >= 0
        && (
          ( // Use code availability if no period
            !codes[v.codeId].availablePeriod
            && (!codes[v.codeId].startDatetime || today >= codes[v.codeId].startDatetime.toDate())
            && (!codes[v.codeId].endDatetime || v.redeemDatetime.toDate() <= codes[v.codeId].endDatetime.toDate())
          )
          || ( // Compare period
            codes[v.codeId].availablePeriod
            && new Date(v.redeemDatetime.toDate().getTime() + codes[v.codeId].availablePeriod) >= today
          )
        )
      )
    ;
    data.isPromo = ownerCodes.length > 0;
  }

  return resultObject(data);
};

const service = {
  init: (req) => {
    redeemCodeModel.init(req);
    redeemRecordModel.init(req);
  },
  insert,
  redeem,
  authorizated,
};

export const callbacks = {
  beforeFilter: (func = "", data) => data,
  afterFilter: (func = "", data) => data,
};

export default service;
