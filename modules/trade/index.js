import { path } from "../moduleFrame.js";
import resultObject, { stateConstant } from "../../utils/resultObject.js";
import assetModule from "./asset/index.js";

export const prefix = "/trade";
const modulePath = prefix;

const module = {
  init: builder => {
    builder("get", path("", modulePath), (req, res) => { res.send("Trade api"); }, true);

    assetModule.init(builder);
  },
};

export default module;
