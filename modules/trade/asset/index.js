import { path, routeInsert } from "../../moduleFrame.js";
import authData from "../../../utils/authData.js";
import resultObject from "../../../utils/resultObject.js";
import transactionService from "../services/transaction.service.js";
import { prefix } from "../index.js";

let modulePath;

// Create transaction & entitlement
const purchase = async (req, res) => {
  transactionService.init(req);
  const { data = {} } = req.body;
  const authUser = authData(req);
  data.owner = authUser.user.id;
  data.platform = authUser.platform;
  data.package = authUser.package;
  const result = await transactionService.commit(data); // Do purchase
  res.json(result);
};

// Update entitlement // TODO
const notifications = async (req, res) => {
  const data = {};
  res.json(data);
};

const init = (builder) => {
  modulePath = `${prefix}/asset`;

  builder("post", path("/purchase", modulePath), purchase, true);
  builder("post", path("/notifications", modulePath), notifications, true);
};

const module = {
  init,
};

export default module;
