import iapService from "../sale/services/iap.service.js";

const service = {
  init: (req) => {
  },
  parseExternalTransaction: async data => {
    const { platform, platformTransaction: transaction } = data.source;
    const newRransaction = await iapService.verify(data, platform, transaction);
    return newRransaction;
  },
};

export default service;
