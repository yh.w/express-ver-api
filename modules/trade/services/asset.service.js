import assetModel, { assetTypes, assetTypeDefault, assetSettings } from "../models/asset.model.js";

const service = {
  init: (req) => {
    assetModel.init(req);
  },
  getAssets: async packageName => {
    const data = await assetModel.get({ where: { field: "package", condition: "==", value: packageName || "general" }}, {});
    if (data?.data?.length === 0) return service.getDefaultAssets();
    const assets = {};
    data.data.map(asset => {
      if (asset.status === "active") {
        assets[asset.type] = asset;
      }
    });
    return assets;
  },
  getDefaultAssets: () => {
    const assets = {};
    const assetSettings = {};
    Object.entries(assetSettings).map(([k, v]) => settings[k] = v.default);
    assetTypes.map(type => assets[type] = {
      type,
      name: type,
      roles: [],
      skus: [],
      ...assetSettings,
      availablePeriod: null,
      meta: {},
      status: "active",
    });
    return assets;
  },
  getExternalAsset: async item => {
    // Create an external item
    const newAsset = {
      type: item.type,
      name: item.name,
      roles: [],
      package: item.meta?.package || "general",
      skus: [item.sku],
      availablePeriod: null,
      isExpired: item.meta?.expireDatetime && new Date() > item.meta.expireDatetime,
      endDatetime: item.meta?.expireDatetime,
      meta: { isExternal: true },
    };
    const data = await assetModel.upsert(newAsset, false);
    return data.data;
  },
  getSkuAsset: async (item, assets) => Object.values(assets).filter(asset => asset?.skus?.indexOf(item.sku) >= 0).shift() || (item.meta?.isExternal ? await service.getExternalAsset(item) : null) || null,
};

export const callback = {
  beforeFilter: (func = "", data) => data,
  afterFilter: (func = "", data) => data,
};

export default service;
