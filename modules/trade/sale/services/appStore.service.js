import axios from "axios";
import jwtDecode from "jwt-decode";
import resultObject, { stateConstant } from "../../../../utils/resultObject.js";

// https://developer.apple.com/documentation/appstoreservernotifications/notification_type

const service = {
  init: (req) => {
  },
  verify: async (data, sandbox = false) => {
    // `data` should be app store receipt
    const receipt = data.transaction.appStoreReceipt;

    // Setup options
    const options = {
      path: `https://${!sandbox ? "buy" : "sandbox"}.itunes.apple.com/verifyReceipt`,
      data: {
        "receipt-data": receipt,
      },
    };
    options.data.password = "e98ca5d457504aaeb8d15e4971ed589c"; // TODO move key to config

    // Request developer api
    const verifyReceipt = await axios.post(options.path, options.data);

    // Verifying receipt
    // https://developer.apple.com/documentation/appstorereceipts/status
    const body = verifyReceipt && verifyReceipt.data ? verifyReceipt.data : {};
    const { status: statusCode } = body;
    switch (statusCode) {
      case 0:
        return resultObject(body);
      // Handle sandbox receipt on production url
      case 21007:
        return await service.verify(data, true);
      // Handle retry status code
      // TODO
      case 21009:
        return resultObject(false, body, stateConstant.ERROR_DATA_INVALID, "Exceeding tries");
      default:
        return resultObject(false, body, stateConstant.ERROR_DATA_INVALID, "Invalid receipt");
    }
  },
  parseNotificationTransaction: notification => {
    // Decode notification
    const data = jwtDecode(notification.signedPayload || "");
    if (data.version !== "2.0") return {}; // Parse v2 only first

    data.data.signedTransactionInfo = jwtDecode(data.data.signedTransactionInfo);
    data.data.signedRenewalInfo = jwtDecode(data.data.signedRenewalInfo);

    return data;
  },
  packToTransaction: (data, externalTransaction) => {
    if (externalTransaction.status !== "success") return data;
    const transaction = externalTransaction.data;

    // Override fields
    data.overrideAmount = (data.data.platformTransaction.priceMicros / 1000000).toString(); // overrideAmount
    data.currency = data.data.platformTransaction.currency; // currency

    // Handle fields, probably contains infinite objects
    transaction.receipt.in_app = transaction.receipt.in_app.length ? [transaction.receipt.in_app[0]] : [];
    transaction.latest_receipt_info = transaction.latest_receipt_info.length ? [transaction.latest_receipt_info[0]] : [];

    // Ovrride items
    data.items = transaction.latest_receipt_info; // latest_receipt_info -> items
    if (data.items.length) {
      data.items[0] = {
        ...data.items[0],
        name: data.data.platformTransaction.alias,
        sku: data.items[0].product_id,
        type:
          data.items[0].subscription_group_identifier !== undefined ? ( // 
            // subscription product
            transaction.pending_renewal_info.auto_renew_status ?
              "periodic"
            :
              "subscription" // subscription_group_identifier = null
          )
          : ("comsumable") // comsumable product // TODO to be tested, comsumable or permanent
        ,
        price: data.overrideAmount,
        meta: {
          isExternal: true,
          expireDatetime: data.items[0].expires_date_ms ? new Date(parseInt(data.items[0].expires_date_ms)) : null,
          package: data.source.package,
        },
      };

      data.transactionDatetime = new Date(parseInt(data.items[0].purchase_date_ms)); // Update transaction date
    }

    data.source.platformTransaction = transaction; // Add back platformTransaction
    return data;
  },
};

export default service;
