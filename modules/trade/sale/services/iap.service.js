import appStoreService from "./appStore.service.js";
import plyStoreService from "./playStore.service.js";

const service = {
  init: (req) => {
  },
  verify: async (data, platform, externalTransaction) => {
    // `data` should be app store or play store objects
    switch (platform) {
      case "ios":
        return appStoreService.packToTransaction(data, await appStoreService.verify(externalTransaction));
      case "android":
        return playStoreService.packToTransaction(data, await playStoreService.verify(externalTransaction));
      default:
        return data; // Do not pack
    }
  },
};

export default service;
