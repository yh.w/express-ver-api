import { google } from "googleapis";
// import googleapiKey from "../config/keys/googleapiKey.js"; // TODO
import resultObject from "../../../../utils/resultObject.js";

const service = {
  init: (req) => {
  },
  // TODO copy from template only
  verify: async data => {
    // Init googleapi
    const androidpublisher = google.androidpublisher('v3');
    const auth = new google.auth.GoogleAuth({
      credentials: googleapiKey, // supporting `keyFilename` or `keyFile`
      scopes: ['https://www.googleapis.com/auth/androidpublisher'],
    });
    const authClient = await auth.getClient();
    // Acquire an auth client, and bind it to all future calls
    google.options({ auth: authClient });

    // `data` should be play store token
    const req = {
      packageName: bundleId,
      token,
    };
    if (type === "product") req.productId = productId;
    else req.subscriptionId = productId;

    const res = await androidpublisher.purchases.subscriptions.get(req);

    const { data: verifiedData } = res;
    return (
      parseInt(data.expiryTimeMillis) >= Date.now() ? // Check expire time
        resultObject(verifiedData)
      :
        resultObject(false, verifiedData, "Expired", true)
    );
  },
};

export default service;

