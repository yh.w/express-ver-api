import mongoose from "mongoose";
import { assetTypes, assetSettings } from "./asset.model.js";
import modelUtils, { validator } from "../../../utils/modelUtils.js";

const { Schema } = mongoose;

export const collection = "entitlements";

export const schema = {
  ownerForeignKey: { // owned by different model
    model: String,
    field: String,
  },
  ownerId: {
    type: String,
  },
  originalTransactionId: { // original transaction
    type: String,
  },
  transactionId: {
    type: String,
  },
  type: { // type of asset, indicate consuming, see products for mappings
    type: String,
    enum: {
      values: assetTypes,
      message: "{VALUE} is not supported",
    },
  },
  sku: {
    type: String,
  },
  ...assetSettings,
  // For customised fields
  meta: {
    type: Schema.Types.Mixed,
    default: {},
  },
};

const events = {
  onPackData: null,
  afterRetrive: null,
};

const associations = {
};

const dataSource = "firestore";

export default modelUtils({
  events,
  associations,
  dataSource,
  collection,
  schema,
});
