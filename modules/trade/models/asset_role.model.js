import mongoose from "mongoose";
import modelUtils, { validator } from "../../../utils/modelUtils.js";

const { Schema } = mongoose;

export const collection = "asset_roles";

export const schema = {
  name: {
    type: String,
  },
  parent: { // TODO as a master group
    type: String,
  },
  permissions: [{ // string, support by mappings build from source code
    type: String,
  }],
  // For customised fields
  meta: {
    type: Schema.Types.Mixed,
    default: {},
  },
};

const events = {
  onPackData: null,
  afterRetrive: null,
};

const associations = {
};

const dataSource = "firestore";

export default modelUtils({
  events,
  associations,
  dataSource,
  collection,
  schema,
});
