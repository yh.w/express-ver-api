/*
 *  Transaction row, suppose no changes made
 *  Changes updated on entitlement
 */
import mongoose from "mongoose";
import modelUtils, { validator } from "../../../utils/modelUtils.js";
import entitlementModel from "./entitlement.model.js";
import { collection as userCollection } from "../../user/models/user.model.js";

const { Schema } = mongoose;

export const collection = "transactions";

export const version = "1.0";
export const transactionActions = ["purchase", "renew", "refund"];
export const ownerForeignKey = {
  model: userCollection,
  field: "id",
};

export const itemSchema = {
  name: { // Display only
    type: String,
  },
  sku: { // external sku, for mapping internal entitlement types
    type: String,
  },
  type: {
    type: String,
    // asset type, default consumable
  },
  price: { // string type for precise
    type: String,
  },
  quantity: {
    type: String,
  },
  meta: {
    type: Schema.Types.Mixed,
  },
};

export const schema = {
  data: { // raw data, json string
    type: Schema.Types.Mixed,
  },
  source: { // parsed data, transaction details
    package: String,
    countryCode: String,
    platform: String,
    platformTransaction: Schema.Types.Mixed, // override `items` on some platforms
  },
  items: [itemSchema],
  amount: { // calculate sum of item price
    type: String,
  },
  overrideAmount: { // Set, if not calculated from items
    type: String,
    default: "",
  },
  currency: {
    type: String,
  },
  action: { // state what transaction does
    type: String,
    enum: {
      values: transactionActions,
      message: "Action: {VALUE} is not supported",
    },
  },
  ownerForeignKey: { // owned by different model
    model: String,
    field: String,
  },
  ownerId: {
    type: String,
  },
  originalTransactionId: {
    type: String,
  },
  transactionDatetime: { // not editable date
    type: Date,
  },
  purchaseDatetime: { // editable date, for display
    type: Date,
  },
  version: { // transaction version
    type: String,
    default: version,
    required: true,
  },
};

const events = {
  onPackData: null,
  afterRetrive: null,
};

const associations = {
};

const dataSource = "firestore";

export default modelUtils({
  events,
  associations,
  dataSource,
  collection,
  schema,
});
