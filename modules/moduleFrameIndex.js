/*
 *  You can directly copying belows to quick build a module with CRUD endpoints
 */
import moduleFrame, { path } from "./moduleFrame.js";

const modulePath = "/demo"; // Should begin with `/` e.g. "/frame"

/*
 *  Should be string or module.service
 *  String: to use default module service apis
 *  module.service: to use custom-defined module service apis
 *    At leaset service apis of, init(), get(), upsert(), delete()
 *      to be corporated with flags from `defaultRoutes`
 *      see moduleFrame.js `service` build
 */
const collection = "demo";

const dataSource = "firestore";
const defaultRoutes = {
  get: true,
  getById: true,
  insert: true,
  insertById: true,
  update: true,
  updateById: true,
  deleteOrSoft: true,
};

// Samples
// Sample service functions
const serviceFuncWrapper = async (req, res) => {
  // 1a. Should call service functions here
  // 1b. Service function should handle `req`, but not `res`
  // 2. Should handle `res` here, act as middlewares ending point
  res.send({ ok: 200 });
};
// Sample index init()
const init = (builder) => {
  builder("get", path("/sample", modulePath), serviceFuncWrapper, false);
};

const module = moduleFrame({
  path: modulePath,
  service: collection,
  dataSource,
  defaultRoutes,
  initCallback: init, // Beside base routes, callback to set extra
  options: {
    authEndpoints: false,
  },
});

export default module;
