import authModel, { authProviders } from "../models/auth.model.js";
import userModel, { removeSensitive } from "../../user/models/user.model.js";
import userService from "../../user/services/user.service.js";
import { validator } from "../../../utils/modelUtils.js";
import callbacksUtils from "../../../utils/callbacksUtils.js";
import firebaseAuth from "../../../utils/firebaseAuth.js";
import devLogger from "../../../utils/devLogger.js";

const defaultAuthProvider = "email"; // First auth provider

const service = {
  init: req => {
    userService.init(req);
  },
  getAuthUser: async params => {
    devLogger.debug(`Auth user params: ${JSON.stringify(params)}`);
    const { type, user, condition = "==", raw = false } = params;
    if (!type) return null;

    const queries = {
      field: type,
      condition,
      value: user[type],
      orderBy: false,
    };
    const found = await userModel.get(
      {
        where: queries,
        orderBy: queries?.orderBy ? queries.orderBy : (!queries?.orderBy ? {} : { field: queries ? queries.field : "createDatetime", sort: "asc" }),
      }, {
        raw: true,
      }
    );
    let data = (found.data || []).shift();

    // Create if not found, e.g. first login
    if (!data) {
      if (params.token.firebase?.sign_in_provider) {
        const newUser = await userService.syncNewUser(firebaseAuth.buildAuthData(user[type].shift().shift(), params.token.firebase.sign_in_provider));
        if (newUser.status !== "success") data = false;
      }
    }

    return raw ? data : removeSensitive(data);
  },
  getAuthUserInAuthUid: async (uid, token = {}) => service.getAuthUser({
    user: { authUid: [[uid]] },
    type: "authUid",
    condition: "in",
    token,
  }),
  authUserParams: async (req, theData) => {
    let data = theData;
    data = await callbacksUtils.fetch(callbacks, "beforeFilter", { state: "authUserParams", data });

    // Set default oauth method, if authUid present
    if (data.authUid && !data.token) data.token = data.authUid; // Support field `token` only
    if (data.token && !data.method) data.method = "google.com";

    // Get provider by data field or method provided
    const methods = Object.values(authModel).filter(v => v.status);
    if (!data.method) data.method = defaultAuthProvider; // default auth provider if not set
    const method = methods.filter(v => v.provider === data.method).shift();
    // if (data.authUid)
    if (!method) return {};

    devLogger.debug(`Checking data: ${JSON.stringify(data)}`);
    const params = {};
    const provider = method.provider || defaultAuthProvider; // default auth provider if not set
    params[method.identity] = data[method.identity];
    params[method.authField] = data[method.authField];

    // Server validation
    let authUser = {};
    userService.init(req);
    devLogger.debug(`Checking method: ${JSON.stringify(method)}`);
    devLogger.debug(`Checking params: ${JSON.stringify(params)}`);
    if (method.identity === "oauth") { // A self oauth, not from 3rd-party providers, e.g. Firebase
      // Search with authUid
      const authUid = params[method.authField]; // TODO verify the token with jwt
      authUser = await service.getAuthUser(authUid);
    } else {
      if (provider === "email") {
        // Parse valid email field value from db
        const email = (params[method.identity] || "").match(validator.emailRegex);
        if (!email) params[method.identity] = "";
      }

      const user = {
        [provider]: params[method.identity], // Map provider field name to identity field
        password: params.password || "",
      };

      authUser = await service.getAuthUser({
        user,
        type: provider,
        raw: true,
      });

      if (!authUser) return {};

      if (
        authUser.status !== "active"
        || !userService.validPassword(authUser, String(user.password))
      ) return {};
    }

    // Delete sensitive
    if (authUser) {
      authUser.oauth = method.provider;
      delete authUser.authUid;
      delete authUser.salt;
      delete authUser.hash;
    }

    authUser = await callbacksUtils.fetch(callbacks, "afterFilter", { state: "authUserParams", data: authUser });
    return authUser;
  },
  validateProvider: (provider, data) => {
    // Filter mapped provider
    const authProviders = authModel;
    const filtered = Object.entries(authProviders).filter(([k, v]) => v.provider === (provider || "email"));
    if (filtered.length < 0 || !filtered[0] || !filtered[0][1]) return false;

    const [authKey, authProvider] = filtered[0];
    let requiredFields = true;
    Object.values(authProvider.requiredFields).map(v => { if (!data[v]) requiredFields = false; });
    return requiredFields ? authKey : false;
  },
  firebaseAdminAuth: async (req, data) => {
    return await firebaseAuth.adminVerifying(req.app, data);
  },
  // For remote services submitting auth request
  remoteAuthApp: (appId) => {
    // TODO should wrapped by customToken
    // TODO `revamp_remoteAuthApp` revise once key store methods
    const remoteAuthApps = {
      providers: {
        firebaseAdmin: "apiKey",
      },
      apps: {
        appId: "firebaseAdmin",
      },
    };
    // TODO `revamp_remoteAuthApp` should check service-side generated token for verifying
    const apiKey = remoteAuthApps.providers[remoteAuthApps.apps[appId]];

    // Build api path
    const exchangeCustomTokenEndpoint = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithCustomToken?key=${apiKey}`;
    return exchangeCustomTokenEndpoint;
  },
};

export const callbacks = {
  beforeFilter: (func = "", data) => data,
  afterFilter: (func = "", data) => data,
};

export default service;
