export const getProviders = () => Object.keys(authProviders);
export const authProviders = {
  // Self providers
  email: {
    status: true,
    identity: "login",
    authField: "password",
    provider: "email",
    requiredFields: ["email", "password"],
  },
  username: {
    status: true,
    identity: "login",
    authField: "password",
    provider: "username",
    requiredFields: ["username", "password"],
  },
  // oauth providers
  password: {
    status: true,
    identity: "login",
    authField: "token",
    provider: "password",
    requiredFields: ["authUid"],
  },
  google: {
    status: true,
    identity: "oauth",
    authField: "token",
    provider: "google.com",
    requiredFields: ["authUid"],
  },
  apple: {
    status: true,
    identity: "oauth",
    authField: "token",
    provider: "apple",
    requiredFields: ["authUid"],
  },
  facebook: {
    status: true,
    identity: "oauth",
    authField: "token",
    provider: "facebook.com",
    requiredFields: ["authUid"],
  },
};
export default authProviders;
