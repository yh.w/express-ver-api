import mongoose from "mongoose";
import modelUtils from "../utils/modelUtils.js";
import resultObject, { stateConstant } from "../utils/resultObject.js";

export const path = (text, modulePath) => `${modulePath}${text}`;

// For base model, service generations
const { Schema } = mongoose;
const schema = new Schema({
  ...modelUtils({}).generalFields,
}, { _id: false });
let baseModel = {};
let service = {};
let moduleService = {};

// Base route methods
export const routeGet = async (req, res, newService = null) => {
  const theService = newService || service;
  theService.init(req);

  const { id } = req.params;
  let where = { field: "id", condition: "==" };
  where.value = id;
  if (typeof where.value === "string") where.condition = "==";
  else if (typeof where.value === "object" && where.value.length > 0) where.condition = "in";
  if (!where.value) where = null;

  const result = await theService.get(where, { req });
  if (newService) return result;
  res.send(result);
};

export const routeInsert = async (req, res, newService = null) => {
  const theService = newService || service;
  theService.init(req);
  const data = req.body;
  data.id = req.params.userId || data.id;
  const result = data.id ? resultObject(false, stateConstant.ERROR_DATA_INVALID) : await theService.upsert(data, { unique: true, req });
  if (newService) return result;
  res.send(result);
};

export const routeUpdate = async (req, res, newService = null) => {
  const theService = newService || service;
  theService.init(req);
  const data = req.body;
  data.id = req.params.id || data.id;
  const result = data.id ? await theService.upsert(data, { unique: true, update: true, req }) : resultObject(false, stateConstant.ERROR_DATA_INVALID);
  if (newService) return result;
  res.send(result);
};

export const routeDeleteOrSoft = async (req, res, newService = null) => {
  const theService = newService || service;
  theService.init(req);

  const data = req.params.id;

  const result = await theService.delete(data);
  if (newService) return result;
  res.send(result);
};

// Basic service methods
export const serviceGet = async (queries, options = {}) => {
  const data = await baseModel.get({
    collectionName: moduleService,
    // Default filter by status
    where: {
      field: "status",
      condition: "==",
      value: "active",
    },
    orderBy: queries?.orderBy ? queries.orderBy : (queries?.orderBy === false ? {} : { field: queries ? queries.field : "createDatetime", sort: "asc" }),
  }, options);
  return data;
};

export const serviceUpsert = async (data = {}, options = {}) => {
  // Prepare data
  const id = data.id;
  if (id) data.id = id;
  else {
    // Delete inproper
    delete data._id;
    delete data.id;
  }

  const result = await baseModel.upsert(data, options);
  return result;
};

export const serviceDelete = async (ids, options = {}) => {
  const data = { id: ids, status: "deleted" };
  const find = await baseModel.get({
    collectionName: moduleService,
    where: {
      field: "id",
      condition: "==",
      value: ids,
    },
  }, options);
  if (find.status !== "success" || find.data.length < 1) {
    // res.send(resultObject(false, stateConstant.ERROR_DATA_INVALID));
    return;
  }

  const result = await baseModel.upsert(data, { update: true });
  return result;
};

const moduleFrame = (params) => {
  // Params avaliable
  const {
    path: modulePath,
    service: paramService,
    dataSource = "firestore",
    defaultRoutes = {
      get: true,
      getById: true,
      insert: true,
      insertById: true,
      update: true,
      updateById: true,
      deleteOrSoft: true,
    },
    initCallback = () => {},
    options = {
      authEndpoints: false, // Enable of authorization protected endpoints
      prefix: { // Endpoints prefix
        get: "",
        insert: "",
        update: "",
        deleteOrSoft: "",
      },
      suffix: { // Endpints suffix, will override endpoints of xxxById(), if set
        get: "",
        insert: "",
        update: "",
        // deleteOrSoft: "", // Do not support that overriding param :id
      },
    },
  } = params;
  moduleService = paramService;

  // Configurate module
  if (typeof moduleService === "string") {
    // Use basic model, service
    baseModel = modelUtils({
      dataSource: dataSource,
      schema: schema,
      collection: moduleService,
    });

    service = {
      init: (req) => {
        baseModel.init(req);
      },
      get: serviceGet,
      upsert: serviceUpsert,
      delete: serviceDelete,
    };
  } else if (moduleService) {
    // Use defined service object
    service = moduleService;
  }

  // Prefix add after `modulePath`
  const getPrefix = options.prefix?.get ? `/${options.prefix.get}` : "";
  const insertPrefix = options.prefix?.insert ? `/${options.prefix.insert}` : "";
  const updatePrefix = options.prefix?.update ? `/${options.prefix.update}` : "";
  const deleteOrSoftPrefix = options.prefix?.deleteOrSoft ? `/${options.prefix.deleteOrSoft}` : "";
  // Suffix add after `path`
  const getSuffix = options.suffix?.get || "";
  const insertSuffix = options.suffix?.insert || "";
  const updateSuffix = options.suffix?.update || "";

  // Basic modele routes, work with `/utils/routerBuilder.js`
  const module = {
    init: builder => {
      if (!modulePath || !service) return;

      // Should before base routes to override
      initCallback(builder);

      const { authEndpoints = false } = options;
      // Base routes
      if (defaultRoutes.get !== false) builder("get", path(`${""}${getSuffix}`, `${getPrefix}${modulePath}`), routeGet, authEndpoints);
      if (defaultRoutes.getById !== false && !getSuffix) builder("get", path("/:id", `${getPrefix}${modulePath}`), routeGet, authEndpoints);
      if (defaultRoutes.insert !== false) builder("post", path(`${""}${insertSuffix}`, `${insertPrefix}${modulePath}`), routeInsert, authEndpoints);
      if (defaultRoutes.insertById !== false && !insertSuffix) builder("post", path("/:id", `${insertPrefix}${modulePath}`), routeInsert, authEndpoints);
      if (defaultRoutes.update !== false) builder("patch", path(`${""}${updateSuffix}`, `${updatePrefix}${modulePath}`), routeUpdate, authEndpoints);
      if (defaultRoutes.updateById !== false && !updateSuffix) builder("patch", path("/:id", `${updatePrefix}${modulePath}`), routeUpdate, authEndpoints);
      if (defaultRoutes.deleteOrSoft !== false) builder("delete", path("/:id", `${deleteOrSoftPrefix}${modulePath}`), routeDeleteOrSoft, authEndpoints);
    },
  };

  return module;
};

export default moduleFrame;
